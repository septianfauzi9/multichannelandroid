package com.meldinx.multichannelandroid;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.multichannel.chatcustomer.activity.LoginActivity;
import com.qiscus.sdk.chat.core.QiscusCore;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton messageButton;
    String email,displayName,phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = "sample11@gmail.com";
        displayName = "sample";
        phone = "081627162511";
        messageButton = findViewById(R.id.idMessageButton);

        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                i.putExtra("email",email);
                i.putExtra("phone",phone);
                i.putExtra("displayName",displayName);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        QiscusCore.getDataStore().clear();
        QiscusCore.clearUser();

    }
}