package com.multichannel.chatcustomer.presenter.imp;

import android.util.Log;

import com.multichannel.chatcustomer.presenter.MainChatMvp;
import com.multichannel.chatcustomer.ui.base.BasePresenter;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.remote.QiscusApi;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by adicatur on 12/24/16.
 */

public class MainChatPresenter extends BasePresenter<MainChatMvp> {

    private Subscription subscription;
    private int page;
    private int limit;
    private Func2<QiscusChatRoom, QiscusChatRoom, Integer> roomComparator = (lhs, rhs) -> rhs.getLastComment().getTime().compareTo(lhs.getLastComment().getTime());


    @Override
    public void onAttachView(MainChatMvp view) {
        super.onAttachView(view);
    }

    @Override
    public void onDetachView() {
        super.onDetachView();
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void getSetupList() {
        checkViewAttached();

        Observable.merge(getApiRooms(limit), getLocalRooms(limit))
                .doOnNext(qiscusChatRooms -> {
                    if (qiscusChatRooms.size() >= limit) {
                        getMvpView().setIsLastPage(false);
                    } else {
                        getMvpView().setIsLastPage(true);
                    }
                })
                .flatMap(Observable::from)
                .filter(qiscusChatRoom -> {
                    if (qiscusChatRoom.getOptions().length() > 0) {
                        return !qiscusChatRoom.getOptions().has("is_resolved");
                    }
                    return true;
                })
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(chatRooms -> {
                    getMvpView().setRoomList(chatRooms);
                }, Throwable::printStackTrace);
    }

    private Observable<List<QiscusChatRoom>> getApiRooms(int limit) {
        return QiscusApi.getInstance().getChatRooms(page, limit, true)
                .flatMap(Observable::from)
                .doOnNext(qiscusChatRoom -> {
                    QiscusCore.getDataStore().addOrUpdate(qiscusChatRoom);
                })
                .toSortedList(roomComparator)
                .subscribeOn(Schedulers.io())
                .onErrorReturn(throwable -> null);
    }

    private Observable<List<QiscusChatRoom>> getLocalRooms(int limit) {
        return QiscusCore.getDataStore().getObservableChatRooms(limit)
                .flatMap(Observable::from)
                .toSortedList(roomComparator)
                .map(qiscusChatRooms -> qiscusChatRooms)
                .subscribeOn(Schedulers.io());
    }

}

