package com.multichannel.chatcustomer.presenter;

import androidx.core.util.Pair;

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;

import java.io.File;
import java.util.List;

import rx.Observable;

public interface RoomChatPresenter {
    void initiateChat(String email, String name, String phone);
    void callRoomData(Long getRoomId);
    void callRoomComments(Long getRoomId);
    void loadComments(int count);
    Observable<Pair<QiscusChatRoom, List<QiscusComment>>> getInitRoomData();
    Observable<List<QiscusComment>> getLocalComments(int count, boolean forceFailedSendingComment);
    void loadOlderRoomComments(QiscusComment qiscusComment);
    Observable<List<QiscusComment>> getCommentsFromNetwork(long lastCommentId);
    List<QiscusComment> cleanFailedComments(List<QiscusComment> qiscusComments);
    boolean isValidOlderComments(List<QiscusComment> qiscusComments, QiscusComment lastQiscusComment);

    void generateComment(String text, String type, boolean isRoomResolved);
    void generateReplyComment(QiscusComment qiscusComment, String comment, boolean isRoomResolved);
    void sendComment(QiscusComment comment);
    void checkFile(File file, String comment, String type, boolean isRoomResolved);
    void notifyLastRead(QiscusComment qiscusComment);
    void setLastChat(boolean isActive);
    void detachView();

    void loadCommentsAfter(QiscusComment comment);
    void setUserTyping(boolean typing);
    void setUserRead(QiscusComment latestSentComment);
    void deleteComment(QiscusComment qiscusComment);
    void deleteComment(List<QiscusComment> qiscusComment);

    QiscusChatRoom getRoomData();
    void generateButtonComment(String payload, String comment, String type);
    void loadUntilComment(QiscusComment qiscusComment);
}
