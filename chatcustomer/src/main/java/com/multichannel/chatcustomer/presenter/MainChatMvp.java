package com.multichannel.chatcustomer.presenter;

import com.multichannel.chatcustomer.ui.base.Mvp;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;

import java.util.List;

/**
 * Created by adicatur on 12/24/16.
 */

public interface MainChatMvp extends Mvp {
    void initCommponent();

    void findView();

    void setProfile();

    void setupList();

    void showPbLoad();

    void dissmissPbLoad();

    void setRoomList(List<QiscusChatRoom> qiscusChatRoom);

    void setIsLastPage(boolean isLastPage);

}
