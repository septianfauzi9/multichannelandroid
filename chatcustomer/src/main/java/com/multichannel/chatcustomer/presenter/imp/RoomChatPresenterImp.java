package com.multichannel.chatcustomer.presenter.imp;

import android.os.Handler;

import androidx.core.util.Pair;

import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.data.remote.QismoWidgetApi;
import com.multichannel.chatcustomer.handler.MultichannelJsonParsingHandler;
import com.multichannel.chatcustomer.presenter.RoomChatPresenter;
import com.multichannel.chatcustomer.ui.view.RoomChatView;
import com.multichannel.chatcustomer.util.QiscusImageUtil;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.local.QiscusCacheManager;
import com.qiscus.sdk.chat.core.data.model.QiscusAccount;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.data.model.QiscusRoomMember;
import com.qiscus.sdk.chat.core.data.remote.QiscusApi;
import com.qiscus.sdk.chat.core.data.remote.QiscusPusherApi;
import com.qiscus.sdk.chat.core.data.remote.QiscusResendCommentHelper;
import com.qiscus.sdk.chat.core.event.QiscusChatRoomEvent;
import com.qiscus.sdk.chat.core.event.QiscusCommentDeletedEvent;
import com.qiscus.sdk.chat.core.event.QiscusCommentReceivedEvent;
import com.qiscus.sdk.chat.core.event.QiscusCommentResendEvent;
import com.qiscus.sdk.chat.core.event.QiscusMqttStatusEvent;
import com.qiscus.sdk.chat.core.event.QiscusUserStatusEvent;
import com.qiscus.sdk.chat.core.util.QiscusAndroidUtil;
import com.qiscus.sdk.chat.core.util.QiscusFileUtil;
import com.qiscus.sdk.chat.core.util.QiscusTextUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.zelory.compressor.Compressor;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

public class RoomChatPresenterImp implements RoomChatPresenter {

    public Date lastActiveDate;
    public boolean isFriendActive = false;
    public boolean isRoomSessional = false;
    private RoomChatView view;
    private Long roomId;
    private QiscusChatRoom room;
    private QiscusAccount qiscusAccount;
    private String opponentEmail;
    private Func2<QiscusComment, QiscusComment, Integer> commentComparator = (lhs, rhs) -> rhs.getTime().compareTo(lhs.getTime());

    private Map<QiscusComment, Subscription> pendingTask;

    private MultichannelJsonParsingHandler jsonParsingHandler;

    private boolean isSessionalCheckClear = false;

    public RoomChatPresenterImp(RoomChatView view) {
        this.view = view;

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        pendingTask = new HashMap<>();

        jsonParsingHandler = new MultichannelJsonParsingHandler();
    }

    @Override
    public void initiateChat(String email, String name,String phone) {
        QismoWidgetApi.getInstance().initiateChat(email, name,phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> view.showError(throwable.getMessage()))
                .subscribe(account -> {
                    callRoomData(Long.parseLong(account.getRoomId()));
                    qiscusAccount = QiscusCore.getQiscusAccount();
                }, Throwable::printStackTrace);
    }

    @Override
    public void callRoomData(Long getRoomId) {
        QiscusChatRoom qiscusChatRoom = QiscusCore.getDataStore().getChatRoom(getRoomId);
        if (qiscusChatRoom == null) {
            QiscusApi.getInstance().getChatRoom(getRoomId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(throwable -> view.showError(throwable.getMessage()))
                    .subscribe(qiscusChatRoom1 -> {
                        //save to database
                        room = qiscusChatRoom1;
                        QiscusCore.getDataStore().addOrUpdate(qiscusChatRoom1);
                        callRoomComments(qiscusChatRoom1.getId());
                        generateComment("Get Started","text", false);
                    }, Throwable::printStackTrace);

        } else {
            room = qiscusChatRoom;
            callRoomComments(qiscusChatRoom.getId());
        }
    }

    @Override
    public void callRoomComments(Long getRoomId) {
        roomId = getRoomId;
        setLastChat(true);
        loadComments(20);
    }

    public void sessionalCheck() {
        isSessionalCheckClear = false;
        QismoWidgetApi.getInstance().checkSessional(QiscusCore.getAppId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sessionalData -> {
                    if (sessionalData.isIsSessional()) {
                        isRoomSessional = true;
                        view.setMessageBoxForQismo(true);
                    } else {
                        isRoomSessional = false;
                        view.setMessageBoxForQismo(false);
                    }
                    isSessionalCheckClear = true;
                }, throwable -> view.showError("Error Sessional Check " + throwable.getMessage()));
    }

    private boolean roomResolved() throws JSONException {
        return jsonParsingHandler.getIsResolved(room.getOptions());
    }

    @Override
    public void loadComments(int count) {
        Observable.merge(getInitRoomData(), getLocalComments(count, true)
                .map(comments -> Pair.create(room, comments)))
                .filter(qiscusChatRoomListPair -> qiscusChatRoomListPair != null)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(qiscusChatRoomListPair -> {
                    if (qiscusChatRoomListPair.first != null) {
                        QiscusComment qiscusComment = qiscusChatRoomListPair.first.getLastComment();
                        if (qiscusComment.getType() == QiscusComment.Type.SYSTEM_EVENT) {
                            String eventLastWord = qiscusComment.getMessage().substring(qiscusComment.getMessage().lastIndexOf(" ") + 1);
                            if (eventLastWord.equals("resolved")) {
                                view.setRoomResolved(true);
                                try {
                                    if (roomResolved()) {
                                        sessionalCheck();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }else{

                    }

                    opponentEmail = Observable.from(room.getMember())
                            .map(QiscusRoomMember::getEmail)
                            .filter(email -> !email.equals(QiscusCore.getQiscusAccount().getEmail()))
                            .first()
                            .toBlocking()
                            .single();

                    view.setParticipants(room.getMember());
                    QiscusPusherApi.getInstance().listenRoom(room);
                    QiscusPusherApi.getInstance().listenUserStatus(opponentEmail);
                })
                .subscribe(roomData -> {
                    view.setRoomData(roomData);
                }, Throwable::printStackTrace);
    }

    @Override
    public Observable<Pair<QiscusChatRoom, List<QiscusComment>>> getInitRoomData() {
        return QiscusApi.getInstance().getChatRoomComments(roomId)
                .doOnError(Throwable::printStackTrace)
                .doOnNext(roomData -> {

                    room = roomData.first;

                    Collections.sort(roomData.second, (lhs, rhs) -> rhs.getTime().compareTo(lhs.getTime()));

                    QiscusCore.getDataStore().addOrUpdate(roomData.first);
                })
                .doOnNext(roomData -> {
                    for (QiscusComment qiscusComment : roomData.second) {
                        QiscusCore.getDataStore().addOrUpdate(qiscusComment);
                    }
                })
                .subscribeOn(Schedulers.io())
                .onErrorReturn(throwable -> null);
    }

    @Override
    public Observable<List<QiscusComment>> getLocalComments(int count, boolean forceFailedSendingComment) {
        return QiscusCore.getDataStore().getObservableComments(roomId, 2 * count)
                .flatMap(Observable::from)
                .toSortedList(commentComparator)
                .map(comments -> {
                    if (comments.size() > count) {
                        return comments.subList(0, count);
                    }
                    return comments;
                })
                .subscribeOn(Schedulers.io());
    }

    @Override
    public void loadOlderRoomComments(QiscusComment qiscusComment) {
        QiscusCore.getDataStore().getObservableOlderCommentsThan(qiscusComment, roomId, 60)
                .flatMap(Observable::from)
                .filter(qiscusComment1 -> qiscusComment.getId() == -1 || qiscusComment1.getId() < qiscusComment.getId())
                .toSortedList(commentComparator)
                .map(comments -> {
                    if (comments.size() >= 20) {
                        return comments.subList(0, 20);
                    }
                    return comments;
                })
                .flatMap(comments -> isValidOlderComments(comments, qiscusComment) ?
                        Observable.from(comments).toSortedList(commentComparator) :
                        getCommentsFromNetwork(qiscusComment.getId()).map(comments1 -> {
                            for (QiscusComment localComment : comments) {
                                if (localComment.getState() <= QiscusComment.STATE_SENDING) {
                                    comments1.add(localComment);
                                }
                            }
                            return comments1;
                        }))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(comments -> {
                    view.setOlderComment(comments);
                }, throwable -> {
                    throwable.printStackTrace();
                });
    }

    @Subscribe
    public void onReceiveRoomEvent(QiscusChatRoomEvent roomEvent) {
        switch (roomEvent.getEvent()) {
            case TYPING:
                view.onUserTyping(roomEvent.isTyping());
                break;
            case DELIVERED:
                QiscusAndroidUtil.runOnUIThread(() -> view.updateLastDeliveredMessage(roomEvent.getCommentId()), 10);
                break;
            case READ:
                QiscusAndroidUtil.runOnUIThread(() -> view.updateLastReadComment(roomEvent.getCommentId()), 10);
                break;
        }
    }

    @Override
    public Observable<List<QiscusComment>> getCommentsFromNetwork(long lastCommentId) {
        return QiscusApi.getInstance().getComments(roomId, lastCommentId)
                .doOnNext(qiscusComment -> {
                    QiscusCore.getDataStore().addOrUpdate(qiscusComment);
                    qiscusComment.setRoomId(roomId);
                })
                .toSortedList(commentComparator)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public List<QiscusComment> cleanFailedComments(List<QiscusComment> qiscusComments) {
        List<QiscusComment> comments = new ArrayList<>();
        for (QiscusComment qiscusComment : qiscusComments) {
            if (qiscusComment.getId() != -1) {
                comments.add(qiscusComment);
            }
        }
        return comments;
    }

    @Override
    public boolean isValidOlderComments(List<QiscusComment> qiscusComments, QiscusComment lastQiscusComment) {
        if (qiscusComments.isEmpty()) return false;

        qiscusComments = cleanFailedComments(qiscusComments);
        boolean containsLastValidComment = qiscusComments.size() <= 0 || lastQiscusComment.getId() == -1;
        int size = qiscusComments.size();

        if (size == 1) {
            return qiscusComments.get(0).getCommentBeforeId() == 0
                    && lastQiscusComment.getCommentBeforeId() == qiscusComments.get(0).getId();
        }

        for (int i = 0; i < size - 1; i++) {
            if (!containsLastValidComment && qiscusComments.get(i).getId() == lastQiscusComment.getCommentBeforeId()) {
                containsLastValidComment = true;
            }

            if (qiscusComments.get(i).getCommentBeforeId() != qiscusComments.get(i + 1).getId()) {
                return false;
            }
        }
        return containsLastValidComment;
    }

    @Override
    public void generateComment(String text, String type, boolean isRoomResolved) {
        String adjustedText = text.replaceAll("\\s+$", "");
        QiscusComment qiscusComment = QiscusComment.generateMessage(roomId, adjustedText);
        if (isRoomResolved) {
            view.setLastCommentWhenRoomResolved(qiscusComment);
            sessionalCheck();
            actionAfterSessionalCheck(qiscusComment);
        } else {
            view.sendingComment(qiscusComment);
            sendComment(qiscusComment);
        }
    }

    private void actionAfterSessionalCheck(QiscusComment qiscusComment) {
        if (isSessionalCheckClear) {
            if (!isRoomSessional) {
                view.sendingComment(qiscusComment);
                sendComment(qiscusComment);
            }
        } else {
            new Handler().postDelayed(() -> actionAfterSessionalCheck(qiscusComment), 200);
        }
    }

    @Override
    public void generateReplyComment(QiscusComment qiscusComment, String comment, boolean isRoomResolved) {
        QiscusComment replyMessage = QiscusComment.generateReplyMessage(roomId, comment, qiscusComment);

        sendComment(replyMessage);
    }

    @Override
    public void sendComment(QiscusComment comment) {
        QiscusApi.getInstance().postComment(comment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(this::commentSuccess)
                .subscribe(commentSent -> {
                    // success
                    if (commentSent.getRoomId() == roomId) {
                        view.onCommentSuccess(commentSent);
                    }
                }, Throwable::printStackTrace);
    }

    @Override
    public void checkFile(File file, String comment, String type, boolean isRoomResolved) {
        if (isRoomResolved) {
            QiscusComment qiscusComment = QiscusComment.generateFileAttachmentMessage(roomId, file.getPath(),
                    comment, file.getName());

            qiscusComment.setDownloading(true);

            view.setLastCommentWhenRoomResolved(qiscusComment);

            sessionalCheck();
        } else {
            compressImage(file, comment, type);
        }
    }

    private void compressImage(File file, String message, String type) {
        File compressedFile = file;

        if (QiscusFileUtil.isImage(file.getPath()) && !file.getName().endsWith(".gif")) {
            try {
                compressedFile = new Compressor(QiscusCore.getApps()).compressToFile(file);
            } catch (NullPointerException | IOException e) {
                view.errorMessage("Can not read file, please make sure that is not corrupted file!");
                return;
            }
        } else {
            compressedFile = QiscusFileUtil.saveFile(compressedFile);
        }

        generateFileAttachment(compressedFile, message, type);
    }

    private void generateFileAttachment(File file, String comment, String type) {
        QiscusComment qiscusComment = QiscusComment.generateFileAttachmentMessage(roomId, file.getPath(),
                comment, file.getName());

        qiscusComment.setDownloading(true);

        view.sendingComment(qiscusComment);

        uploadFile(file, qiscusComment, comment, type);
    }

    public void uploadFile(File compressedFile, QiscusComment qiscusComment, String comment, String type) {
        Subscription subscription = QiscusApi.getInstance()
                .uploadFile(compressedFile, total -> {
                    qiscusComment.setProgress((int) total);
                })
                .flatMap(uri -> {
                    QiscusComment qiscusComment2 = QiscusComment.generateFileAttachmentMessage(roomId, uri.toString(),
                            comment, compressedFile.getName());
                    return QiscusApi.getInstance().postComment(qiscusComment2);
                })
                .doOnError(throwable -> commentFail(throwable, qiscusComment))
                .doOnNext(qiscusComment12 -> {
                    qiscusComment.setDownloading(false);
                    commentSuccess(qiscusComment12);
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(qiscusComment1 -> {
                    if (QiscusFileUtil.isImage(compressedFile.getName())) {
                        String filepath = QiscusFileUtil.generateFilePath(compressedFile.getName());
                        QiscusCore.getDataStore()
                                .addOrUpdateLocalPath(qiscusComment1.getRoomId(),
                                        qiscusComment1.getId(), filepath);
                        QiscusImageUtil.addImageToGallery(QiscusFileUtil.saveFile(compressedFile));
                        view.onCommentSuccess(qiscusComment1);
                    } else {
                        QiscusCore.getDataStore()
                                .addOrUpdateLocalPath(qiscusComment1.getRoomId(),
                                        qiscusComment1.getId(), compressedFile.getAbsolutePath());
                        QiscusImageUtil.addImageToGallery(compressedFile);
                        view.onCommentSuccess(qiscusComment1);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    commentFail(throwable, qiscusComment);
                    if (qiscusComment.getRoomId() == room.getId()) {
                        view.onFailedSendComment(qiscusComment);
                    }
                });

        pendingTask.put(qiscusComment, subscription);
    }

    private void commentFail(Throwable throwable, QiscusComment qiscusComment) {
        pendingTask.remove(qiscusComment);
        if (!QiscusCore.getDataStore().isContains(qiscusComment)) { //Have been deleted
            return;
        }

        int state = QiscusComment.STATE_PENDING;
        if (mustFailed(throwable, qiscusComment)) {
            qiscusComment.setDownloading(false);
            state = QiscusComment.STATE_FAILED;
        }

        //Kalo ternyata comment nya udah sukses dikirim sebelumnya, maka ga usah di update
        QiscusComment savedQiscusComment = QiscusCore.getDataStore().getComment(qiscusComment.getUniqueId());
        if (savedQiscusComment != null && savedQiscusComment.getState() > QiscusComment.STATE_SENDING) {
            return;
        }

        //Simpen statenya
        qiscusComment.setState(state);
        QiscusCore.getDataStore().addOrUpdate(qiscusComment);
    }

    private boolean mustFailed(Throwable throwable, QiscusComment qiscusComment) {
        //Error response from server
        //Means something wrong with server, e.g user is not member of these room anymore
        return ((throwable instanceof HttpException && ((HttpException) throwable).code() >= 400) ||
                //if throwable from JSONException, e.g response from server not json as expected
                (throwable instanceof JSONException) ||
                // if attachment type
                qiscusComment.isAttachment());
    }

    public void resendComment(QiscusComment qiscusComment) {
        qiscusComment.setState(QiscusComment.STATE_SENDING);
        qiscusComment.setTime(new Date());
        if (qiscusComment.isAttachment()) {
            resendFile(qiscusComment);
        } else {
            sendComment(qiscusComment);
        }
    }

    private void resendFile(QiscusComment qiscusComment) {
        if (qiscusComment.getAttachmentUri().toString().startsWith("http")) { //We forward file message
            forwardFile(qiscusComment);
            return;
        }

        File file = new File(qiscusComment.getAttachmentUri().toString());
        if (!file.exists()) { //File have been removed, so we can not upload it anymore
            qiscusComment.setDownloading(false);
            qiscusComment.setState(QiscusComment.STATE_FAILED);
            QiscusCore.getDataStore().addOrUpdate(qiscusComment);
            view.onFailedSendComment(qiscusComment);
            return;
        }

        qiscusComment.setDownloading(true);
        qiscusComment.setProgress(0);
        Subscription subscription = QiscusApi.getInstance()
                .uploadFile(file, percentage -> qiscusComment.setProgress((int) percentage))
                .doOnSubscribe(() -> QiscusCore.getDataStore().addOrUpdate(qiscusComment))
                .flatMap(uri -> {
                    qiscusComment.updateAttachmentUrl(uri.toString());
                    return QiscusApi.getInstance().postComment(qiscusComment);
                })
                .doOnNext(commentSend -> {
                    QiscusCore.getDataStore()
                            .addOrUpdateLocalPath(commentSend.getRoomId(), commentSend.getId(), file.getAbsolutePath());
                    qiscusComment.setDownloading(false);
                    commentSuccess(commentSend);
                })
                .doOnError(throwable -> commentFail(throwable, qiscusComment))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(commentSend -> {
                    if (commentSend.getRoomId() == room.getId()) {
                        view.onCommentSuccess(commentSend);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    if (qiscusComment.getRoomId() == room.getId()) {
                        view.onFailedSendComment(qiscusComment);
                    }
                });

        pendingTask.put(qiscusComment, subscription);
    }

    private void forwardFile(QiscusComment qiscusComment) {
        qiscusComment.setProgress(100);
        Subscription subscription = QiscusApi.getInstance().postComment(qiscusComment)
                .doOnSubscribe(() -> QiscusCore.getDataStore().addOrUpdate(qiscusComment))
                .doOnNext(commentSend -> {
                    qiscusComment.setDownloading(false);
                    commentSuccess(commentSend);
                })
                .doOnError(throwable -> {
                    qiscusComment.setDownloading(false);
                    commentFail(throwable, qiscusComment);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(commentSend -> {
                    if (commentSend.getRoomId() == room.getId()) {
                        view.onCommentSuccess(commentSend);
                    }
                }, throwable -> {
                    throwable.printStackTrace();
                    if (qiscusComment.getRoomId() == room.getId()) {
                        view.onFailedSendComment(qiscusComment);
                    }
                });

        pendingTask.put(qiscusComment, subscription);
    }

    @Override
    public void notifyLastRead(QiscusComment qiscusComment) {
        if (qiscusComment != null) {
            QiscusPusherApi.getInstance()
                    .setUserRead(qiscusComment.getRoomId(), qiscusComment.getId());
        }
    }

    @Override
    public void setLastChat(boolean isActive) {
        if (roomId != null && roomId != 0) {
            QiscusCacheManager.getInstance().setLastChatActivity(isActive, roomId);
        }
    }

    @Subscribe
    public void onUserStatusChanged(QiscusUserStatusEvent event) {
        isFriendActive = event.isOnline(); // true if user is online
        lastActiveDate = event.getLastActive(); // Date of last active user
        view.setOnlineStatus(event.isOnline(), event.getLastActive());
    }

    @Subscribe
    public void onMqttEvent(QiscusMqttStatusEvent event) {
        view.onRealtimeStatusChanged(event == QiscusMqttStatusEvent.CONNECTED);
    }

    @Subscribe
    public void handleRetryCommentEvent(QiscusCommentResendEvent event) {
        if (event.getQiscusComment().getRoomId() == room.getId()) {
            QiscusAndroidUtil.runOnUIThread(() -> {
                if (view != null) {
                    view.refreshComment(event.getQiscusComment());
                }
            });
        }
    }

    @Subscribe
    public void handleDeleteCommentEvent(QiscusCommentDeletedEvent event) {
        if (event.getQiscusComment().getRoomId() == room.getId()) {
            QiscusAndroidUtil.runOnUIThread(() -> {
                if (view != null) {
                    if (event.isHardDelete()) {
                        view.onCommentDeleted(event.getQiscusComment());
                    } else {
                        view.refreshComment(event.getQiscusComment());
                    }
                }
            });
        }
    }

    @Subscribe
    public void onCommentReceivedEvent(QiscusCommentReceivedEvent event) {
        if (event.getQiscusComment().getRoomId() == roomId) {
            onGotNewComment(event.getQiscusComment());
        }
    }

    private void onGotNewComment(QiscusComment qiscusComment) {
        if (qiscusComment.getSenderEmail().equalsIgnoreCase(QiscusCore.getQiscusAccount().getEmail())) {
            QiscusAndroidUtil.runOnBackgroundThread(() -> commentSuccess(qiscusComment));
        } else {
            if (qiscusComment.getRoomId() == roomId) {
                QiscusAndroidUtil.runOnBackgroundThread(() -> {
                    if (!qiscusComment.getSenderEmail().equalsIgnoreCase(qiscusAccount.getEmail())
                            && QiscusCacheManager.getInstance().getLastChatActivity().first) {
                        QiscusPusherApi.getInstance().setUserRead(roomId, qiscusComment.getId());
                    }
                });
                QiscusAndroidUtil.runOnUIThread(() -> view.onNewComment(qiscusComment));
            }
        }
    }

    private void commentSuccess(QiscusComment qiscusComment) {
        pendingTask.remove(qiscusComment);
        qiscusComment.setState(QiscusComment.STATE_ON_QISCUS);
        QiscusComment savedQiscusComment = QiscusCore.getDataStore().getComment(qiscusComment.getUniqueId());
        if (savedQiscusComment != null && savedQiscusComment.getState() > qiscusComment.getState()) {
            qiscusComment.setState(savedQiscusComment.getState());
        }
        QiscusCore.getDataStore().addOrUpdate(qiscusComment);
    }

    private void clearUnreadCount() {
        if (room != null) {
            room.setUnreadCount(0);
            room.setLastComment(null);
            QiscusCore.getDataStore().addOrUpdate(room);
        }
    }

    @Override
    public void detachView() {
        clearUnreadCount();
        EventBus.getDefault().unregister(this);
        QiscusPusherApi.getInstance().unListenRoom(room);
        QiscusPusherApi.getInstance().unListenUserStatus(opponentEmail);
        room = null;
    }

    @Override
    public void loadCommentsAfter(QiscusComment comment) {
        QiscusApi.getInstance().getCommentsAfter(room.getId(), comment.getId())
                .doOnNext(qiscusComment -> qiscusComment.setRoomId(room.getId()))
                .toSortedList(commentComparator)
                .doOnNext(Collections::reverse)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(comments -> {
                    if (view != null) {
                        view.onLoadMore(comments);
                    }
                }, Throwable::printStackTrace);
    }

    @Override
    public void setUserTyping(boolean typing) {
        if (roomId != null && roomId != 0) {
            QiscusPusherApi.getInstance().setUserTyping(roomId, typing);
        }
    }

    @Override
    public void setUserRead(QiscusComment latestSentComment) {
        if (roomId != null && roomId != 0) {
            QiscusPusherApi.getInstance().setUserRead(roomId, latestSentComment.getId());
        }
    }

    @Override
    public void deleteComment(QiscusComment qiscusComment) {
        cancelPendingComment(qiscusComment);
        QiscusResendCommentHelper.cancelPendingComment(qiscusComment);

        // this code for delete from local
        QiscusAndroidUtil.runOnBackgroundThread(() -> QiscusCore.getDataStore().delete(qiscusComment));
        if (view != null) {
            view.dismissLoading();
            view.onCommentDeleted(qiscusComment);
        }
        Observable.from(new QiscusComment[]{qiscusComment})
                .map(QiscusComment::getUniqueId)
                .toList()
                .flatMap(uniqueIds -> QiscusApi.getInstance().deleteComments(uniqueIds, true))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(deletedComments -> {
                    if (view != null) {
                        view.dismissLoading();
                        view.onCommentDeleted(qiscusComment);
                    }
                }, throwable -> {
                    if (view != null) {
                        view.dismissLoading();
                        view.showError(QiscusTextUtil.getString(R.string.failed_to_delete_messages));
                    }

                });
    }

    @Override
    public void deleteComment(List<QiscusComment> qiscusComment) {
        for (QiscusComment deletedComment : qiscusComment) {
            cancelPendingComment(deletedComment);
            QiscusResendCommentHelper.cancelPendingComment(deletedComment);

            // this code for delete from local
            QiscusAndroidUtil.runOnBackgroundThread(() -> QiscusCore.getDataStore().delete(deletedComment));
            if (view != null) {
                view.dismissLoading();
                view.onCommentDeleted(deletedComment);
            }
        }
        Observable.from(qiscusComment)
                .map(QiscusComment::getUniqueId)
                .toList()
                .flatMap(uniqueIds -> QiscusApi.getInstance().deleteComments(uniqueIds, true))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(deletedComments -> {
                }, throwable -> {
                    if (view != null) {
                        view.dismissLoading();
                        view.showError(QiscusTextUtil.getString(R.string.failed_to_delete_messages));
                    }

                });
    }

    public void cancelPendingComment(QiscusComment qiscusComment) {
        if (pendingTask.containsKey(qiscusComment)) {
            Subscription subscription = pendingTask.get(qiscusComment);
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
            pendingTask.remove(qiscusComment);
        }
    }

    @Override
    public QiscusChatRoom getRoomData() {
        return room;
    }

    @Override
    public void loadUntilComment(QiscusComment qiscusComment) {
        QiscusCore.getDataStore().getObservableCommentsAfter(qiscusComment, room.getId())
                .map(comments -> comments.contains(qiscusComment) ? comments : new ArrayList<QiscusComment>())
                .doOnNext(qiscusComments -> {
                    if (qiscusComments.isEmpty()) {
                        QiscusAndroidUtil.runOnUIThread(() -> {
                            if (view != null) {
                                view.showError(QiscusTextUtil.getString(R.string.qiscus_message_too_far));
                            }
                        });
                    }
                })
                .flatMap(Observable::from)
                .toSortedList(commentComparator)
                .flatMap(comments -> isValidChainingComments(comments) ?
                        Observable.from(comments).toSortedList(commentComparator) :
                        Observable.just(new ArrayList<QiscusComment>()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(comments -> {
                    if (view != null) {
                        view.showCommentsAndScrollToTop(comments);
                    }
                }, Throwable::printStackTrace);
    }

    @Override
    public void generateButtonComment(String payload, String comment, String type) {
        QiscusComment qiscusComment = QiscusComment.generatePostBackMessage(roomId, comment, payload);

        sendComment(qiscusComment);
    }

    private boolean isValidChainingComments(List<QiscusComment> qiscusComments) {
        qiscusComments = cleanFailedComments(qiscusComments);
        int size = qiscusComments.size();
        for (int i = 0; i < size - 1; i++) {
            if (qiscusComments.get(i).getCommentBeforeId() != qiscusComments.get(i + 1).getId()) {
                return false;
            }
        }
        return true;
    }
}