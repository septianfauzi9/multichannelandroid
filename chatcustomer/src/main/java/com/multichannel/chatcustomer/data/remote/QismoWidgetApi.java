package com.multichannel.chatcustomer.data.remote;

import com.google.gson.JsonObject;
import com.multichannel.chatcustomer.data.local.DataLocal;
import com.multichannel.chatcustomer.data.pojo.Account;
import com.multichannel.chatcustomer.data.pojo.SessionalData;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.remote.QiscusApi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by adicatur on 12/24/16.
 */

public enum QismoWidgetApi {

    INSTANCE;
    private final String baseUrl = "https://qismo.qiscus.com/";
    private Api api;
    private String origin = "Android";

    QismoWidgetApi() {

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);

        api = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(clientBuilder.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api.class);
    }

    public static QismoWidgetApi getInstance() {
        return INSTANCE;
    }

    public Observable<Account> initiateChat(String email, String name,String phone) {
        JsonObject JsonPhone = new JsonObject();
        JsonPhone.addProperty("phone",phone);
        DataLocal dataLocal = new DataLocal(QiscusCore.getApps().getApplicationContext());
        return QiscusApi.getInstance().requestNonce()
                .flatMap(qiscusNonce -> api.initiateChat(QiscusCore.getAppId(), email, name, qiscusNonce.getNonce(), origin,JsonPhone.toString())
                        .map(jsonElement -> jsonElement.getAsJsonObject().get("data").getAsJsonObject())
                        .map(this::parseAccount)
                        .doOnNext(dataLocal::setAccount)
                        .flatMap(account -> QiscusCore.setUserAsObservable(account.getIdentityToken()))
                        .map(qiscusAccount -> dataLocal.getAccount()));
    }

    public Observable<SessionalData> checkSessional(String appId) {
        DataLocal dataLocal = new DataLocal(QiscusCore.getApps().getApplicationContext());
        return api.sessionalCheck(appId)
                        .map(jsonElement -> jsonElement.getAsJsonObject().get("data").getAsJsonObject())
                        .map(this::parseSessional)
                        .doOnNext(dataLocal::setSessional);
    }

    private Account parseAccount(JsonObject jsonObject) {
        Account account = new Account();
        account.setIdentityToken(jsonObject.getAsJsonObject().get("identity_token").getAsString());
        account.setRoomId(jsonObject.getAsJsonObject().get("customer_room").getAsJsonObject().get("room_id").getAsString());
        return account;
    }

    private SessionalData parseSessional(JsonObject jsonObject) {
        SessionalData sessional = new SessionalData();
        sessional.setIsSessional(jsonObject.getAsJsonObject().get("is_sessional").getAsBoolean());

        return sessional;
    }
}
