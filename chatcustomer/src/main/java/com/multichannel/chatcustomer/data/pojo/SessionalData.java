package com.multichannel.chatcustomer.data.pojo;

import com.google.gson.annotations.SerializedName;

public class SessionalData {

	@SerializedName("is_sessional")
	private boolean isSessional;

	public void setIsSessional(boolean isSessional){
		this.isSessional = isSessional;
	}

	public boolean isIsSessional(){
		return isSessional;
	}

	@Override
 	public String toString(){
		return 
			"SessionalData{" +
			"is_sessional = '" + isSessional + '\'' + 
			"}";
		}
}