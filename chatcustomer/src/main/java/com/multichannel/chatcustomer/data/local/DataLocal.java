package com.multichannel.chatcustomer.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.multichannel.chatcustomer.data.pojo.Account;
import com.multichannel.chatcustomer.data.pojo.Sessional;
import com.multichannel.chatcustomer.data.pojo.SessionalData;

public class DataLocal {
    private SharedPreferences sharedPreferences;

    public DataLocal(Context context) {
        sharedPreferences = context.getSharedPreferences("local", Context.MODE_PRIVATE);
    }

    public void setAccount(Account account) {
        Gson gson = new Gson();
        String json = gson.toJson(account);
        sharedPreferences.edit()
                .putString("account", json)
                .apply();
    }

    public Account getAccount() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("account", "");
        return gson.fromJson(json, Account.class);
    }

    public void setSessional(SessionalData sessionalData) {
        Gson gson = new Gson();
        String json = gson.toJson(sessionalData);
        sharedPreferences.edit()
                .putString("sessional", json)
                .apply();
    }

    public SessionalData getSessional() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("sessional", "");
        return gson.fromJson(json, SessionalData.class);
    }

}
