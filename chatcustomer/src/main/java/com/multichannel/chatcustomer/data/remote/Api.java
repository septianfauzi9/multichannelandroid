package com.multichannel.chatcustomer.data.remote;

import com.google.gson.JsonElement;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by adicatur on 12/24/16.
 */

public interface Api {

    @FormUrlEncoded
    @POST("api/v2/qiscus/initiate_chat")
    Observable<JsonElement> initiateChat(@Field("app_id") String appId,
                                         @Field("user_id") String email,
                                         @Field("name") String name,
                                         @Field("nonce") String nonce,
                                         @Field("origin") String origin,
                                         @Field("user_properties") String phone);

    @GET("{appCode}/get_session")
    Observable<JsonElement> sessionalCheck(@Path("appCode") String appCode);

}
