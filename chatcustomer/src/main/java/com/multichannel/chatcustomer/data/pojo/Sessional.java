package com.multichannel.chatcustomer.data.pojo;

import com.google.gson.annotations.SerializedName;

public class Sessional {

	@SerializedName("data")
	private SessionalData data;

	public void setData(SessionalData data){
		this.data = data;
	}

	public SessionalData getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"Sessional{" +
			"data = '" + data + '\'' + 
			"}";
		}
}