/*
 * **************************************************
 * Created by m.Husein97 @EggSoft on 4/11/19 9:49 PM
 * Copyright (c) 2019 . All rights reserved.
 * Last modified 4/7/19 8:27 PM
 * **************************************************
 */

package com.multichannel.chatcustomer.database;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    private SharedPreferences sp;
    private SharedPreferences.Editor spe;
    private Context context;

    public SharedPref(Context ctx) {
        sp = ctx.getSharedPreferences("com.multichannel.chatcustomer", Context.MODE_PRIVATE);
        spe = sp.edit();
    }


    public void resetAll() {
        setProviderAuthorities("");
    }

    public void setProviderAuthorities(String providerAuthorities) {
        spe.putString("providerAuthorities", providerAuthorities);
        spe.commit();
    }

    public String getProviderAuthorities() {

        return sp.getString("providerAuthorities", "");
    }

}

