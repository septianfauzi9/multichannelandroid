package com.multichannel.chatcustomer.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * @author Yuana andhikayuana@gmail.com
 * @since Aug, Tue 14 2018 15.21
 **/
public class AppFirebaseInstanceIdService extends QiscusFirebaseIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Log.d("FirebaseIdService", "Fcm token: " + FirebaseInstanceId.getInstance().getToken());

        //TODO Application part here
    }
}
