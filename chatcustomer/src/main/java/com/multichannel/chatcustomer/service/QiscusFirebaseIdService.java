package com.multichannel.chatcustomer.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.qiscus.sdk.chat.core.QiscusCore;

public class QiscusFirebaseIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        QiscusCore.setFcmToken(refreshedToken);
    }
}
