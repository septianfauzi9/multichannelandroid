package com.multichannel.chatcustomer.service;

import com.google.firebase.messaging.RemoteMessage;

/**
 * @author Yuana andhikayuana@gmail.com
 * @since Aug, Tue 14 2018 15.23
 **/
public class AppFirebaseMessagingService extends QiscusFirebaseService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (QiscusFirebaseService.handleMessageReceived(remoteMessage)) { // SDK PN, contains key qiscus_sdk
            return;
        }
    }
}
