package com.multichannel.chatcustomer;

import android.content.Context;

import com.multichannel.chatcustomer.util.Constant;
import com.multichannel.chatcustomer.util.PushNotificationUtil;
import com.qiscus.sdk.chat.core.QiscusCore;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;


public class QismoWidget extends MultiDexApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //init Chat

        ChatCustomer.init(this, Constant.QISCUS_APP_ID);

        ChatCustomer.initProviderAuthority(this, getString(R.string.provider_authority));

        QiscusCore.getChatConfig().enableDebugMode(true)
                .setNotificationListener(PushNotificationUtil::showNotification)
                .setEnableFcmPushNotification(true);
    }
}
