package com.multichannel.chatcustomer.handler;

import android.annotation.SuppressLint;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class RepliedJsonParsingHandler {

    private static JsonParsingHandler handler;
    private JSONObject json = new JSONObject();
    private long repliedId;
    private boolean repliedIsDeleted = false;
    private String repliedMessage;
    private String repliedUserName;
    private String repliedType;

    //Extra Payload
    private String repliedImageUrl;
    private String repliedCaption;
    private String repliedFileName;

    public static JsonParsingHandler init() {
        if (handler == null) {
            handler = new JsonParsingHandler();
        }
        return handler;
    }

    public JSONObject setJson(File file, String repliedCaption, String repliedImageUrl) {
        try {
            json.put("url", "https://" + repliedImageUrl)
                    .put("caption", repliedCaption)
                    .put("file_name", file.getName());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @SuppressLint("NewApi")
    public String captionFile(JSONObject json) throws JSONException {
        this.json = json.getJSONObject("replied_comment_payload");
        repliedCaption =  this.json.optString("caption");
        return repliedCaption;
    }


    @SuppressLint("NewApi")
    public String getUrlFile(JSONObject json) throws JSONException {
        String repliedCommentPayload = json.getString("replied_comment_payload");
        JSONObject repliedComment = new JSONObject(repliedCommentPayload);
        repliedImageUrl =  repliedComment.optString("url");
        return repliedImageUrl;
    }

    @SuppressLint("NewApi")
    public String getFileName(JSONObject json) throws JSONException {
        String repliedCommentPayload = json.getString("replied_comment_payload");
        JSONObject repliedComment = new JSONObject(repliedCommentPayload);
        repliedFileName =  repliedComment.optString("file_name");
        return repliedFileName;
    }

    @SuppressLint("NewApi")
    public Long getRepliedMessageId(JSONObject json) throws JSONException {
        repliedId =  json.optLong("replied_comment_id");
        return repliedId;
    }

    @SuppressLint("NewApi")
    public boolean getIsRepliedMessageDeleted(JSONObject json) throws JSONException {
        repliedIsDeleted =  json.optBoolean("replied_comment_is_deleted");
        return repliedIsDeleted;
    }

    @SuppressLint("NewApi")
    public String getRepliedMessageText(JSONObject json) throws JSONException {
        repliedMessage =  json.optString("replied_comment_message");
        return repliedMessage;
    }

    @SuppressLint("NewApi")
    public String getRepliedMessageUserName(JSONObject json) throws JSONException {
        repliedUserName =  json.optString("replied_comment_sender_username");
        return repliedUserName;
    }

    @SuppressLint("NewApi")
    public String getRepliedMessageType(JSONObject json) throws JSONException {
        repliedType =  json.optString("replied_comment_type");
        return repliedType;
    }
}
