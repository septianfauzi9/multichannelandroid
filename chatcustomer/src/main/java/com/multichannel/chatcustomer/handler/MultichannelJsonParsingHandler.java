package com.multichannel.chatcustomer.handler;

import android.annotation.SuppressLint;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class MultichannelJsonParsingHandler {

    private static JsonParsingHandler handler;
    private JSONObject json = new JSONObject();
    private boolean isResolved = false;
    private String notes;

    public static JsonParsingHandler init() {
        if (handler == null) {
            handler = new JsonParsingHandler();
        }
        return handler;
    }

    @SuppressLint("NewApi")
    public String getNotes(JSONObject json) throws JSONException {
        notes =  json.optString("notes");
        return notes;
    }

    @SuppressLint("NewApi")
    public boolean getIsResolved(JSONObject json) throws JSONException {
        isResolved =  json.optBoolean("is_resolved");
        return isResolved;
    }
}
