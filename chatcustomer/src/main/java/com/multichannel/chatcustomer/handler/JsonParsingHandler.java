package com.multichannel.chatcustomer.handler;

import android.annotation.SuppressLint;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class JsonParsingHandler {

    private static JsonParsingHandler handler;
    private JSONObject json = new JSONObject();
    private String url = null;
    private String caption = null;
    private String nameFile = null;

    public static JsonParsingHandler init() {
        if (handler == null) {
            handler = new JsonParsingHandler();
        }
        return handler;
    }

    public JSONObject setJson(File file, String caption, String url) {
        try {
            json.put("url", "https://" + url)
                    .put("caption", caption)
                    .put("file_name", file.getName());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @SuppressLint("NewApi")
    public String getCaptionFile(JSONObject json) throws JSONException {
        caption =  json.optString("caption");
        return caption;
    }


    @SuppressLint("NewApi")
    public String getUrlFile(JSONObject json) throws JSONException {
        url =  json.optString("url");
        return url;
    }

    @SuppressLint("NewApi")
    public String getNameFile(JSONObject json) throws JSONException {
        nameFile =  json.optString("file_name");
        return nameFile;
    }

}
