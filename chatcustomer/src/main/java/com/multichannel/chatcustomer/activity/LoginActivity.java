package com.multichannel.chatcustomer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.multichannel.chatcustomer.ChatCustomer;
import com.multichannel.chatcustomer.ui.activity.RoomChatActivity;
import com.multichannel.chatcustomer.util.PermissionUtil;
import com.qiscus.sdk.chat.core.QiscusCore;

//import android.ut

public class LoginActivity extends AppCompatActivity {

    private EditText mEditUserEmail;
    private EditText mEditDisplayName;
    private LinearLayout mLayoutLogin;
    private TextView mTextLogin;
    private ProgressBar mProgressBar;
    private ImageView mImageLogin;
    private String email;
    private String displayName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void checkPermissions() {
        int PERMISSION_ALL = 101;
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        if (!PermissionUtil.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    private void init() {
        if (QiscusCore.hasSetupUser()) {
            Intent i = new Intent(this, RoomChatActivity.class);
            i.putExtra(com.multichannel.chatcustomer.util.Constant.ACOUNT_DATA, QiscusCore.getQiscusAccount());
            i.putExtra("name", QiscusCore.getQiscusAccount().getUsername());
            i.putExtra("email", QiscusCore.getQiscusAccount().getEmail());
            startActivity(i);
//            finish();
        }else{
            initiateChat();
        }

//        mEditUserEmail = findViewById(R.id.et_user_email);
//        mEditDisplayName = findViewById(R.id.et_display_name);
//        mLayoutLogin = findViewById(R.id.login);
//        mTextLogin = findViewById(R.id.tv_start);
//        mProgressBar = findViewById(R.id.progress_bar);
//        mImageLogin = findViewById(R.id.img);
//
//        mLayoutLogin.setOnClickListener(v -> initiateChat());
    }

    private void initiateChat() {
        Intent i = getIntent();
//        mTextLogin.setVisibility(View.GONE);
//        mImageLogin.setVisibility(View.GONE);
//        mProgressBar.setVisibility(View.VISIBLE);
        String userEmail = i.getStringExtra("email").toLowerCase();
        String userDisplayName = i.getStringExtra("displayName");
        String phone = i.getStringExtra("phone");
        ChatCustomer.initiateChat(this, userEmail, userDisplayName, phone, new ChatCustomer.InitiateCallback() {
            @Override
            public void onSuccess() {
                checkPermissions();
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
