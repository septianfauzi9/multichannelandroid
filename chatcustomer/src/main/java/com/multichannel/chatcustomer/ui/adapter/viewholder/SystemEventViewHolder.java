package com.multichannel.chatcustomer.ui.adapter.viewholder;

import android.util.Log;
import android.view.View;

import com.multichannel.chatcustomer.ui.adapter.OnItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnLongItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;

public class SystemEventViewHolder extends RoomChatAdapter.ViewHolder {

    public SystemEventViewHolder(View itemView, OnItemClickListener onItemClickListener,
                                 OnLongItemClickListener onLongItemClickListener) {
        super(itemView, onItemClickListener, onLongItemClickListener);
    }
}
