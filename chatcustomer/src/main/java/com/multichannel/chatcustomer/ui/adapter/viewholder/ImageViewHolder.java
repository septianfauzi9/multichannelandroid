package com.multichannel.chatcustomer.ui.adapter.viewholder;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.ui.activity.ShowImageActivity;
import com.multichannel.chatcustomer.ui.adapter.OnItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnLongItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;
import com.multichannel.chatcustomer.util.QiscusImageUtil;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.data.remote.QiscusApi;
import com.qiscus.sdk.chat.core.util.QiscusAndroidUtil;
import com.qiscus.sdk.chat.core.util.QiscusFileUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ImageViewHolder extends RoomChatAdapter.ViewHolder {

    private ImageView mImageComment;
    private ImageView mImageDownload;
    private TextView mTextDownloadProgress;
    private ProgressBar mProgressImg;
    private RelativeLayout mLayoutCoverImg;
    private RelativeLayout mLayoutDownload;
    private CardView mCardImage;
    private RequestOptions requestOptions;
    private Context context;
    private JSONObject obj;

    public ImageViewHolder(@NonNull View itemView, Context context, OnItemClickListener onItemClickListener,
                           OnLongItemClickListener onLongItemClickListener) {
        super(itemView, onItemClickListener, onLongItemClickListener);
        mImageComment = itemView.findViewById(R.id.iv_chat_comment);
        mImageDownload = itemView.findViewById(R.id.iv_download);
        mTextDownloadProgress = itemView.findViewById(R.id.tv_progress);
        mProgressImg = itemView.findViewById(R.id.pb_img);
        mLayoutCoverImg = itemView.findViewById(R.id.layout_img_cover);
        mLayoutDownload = itemView.findViewById(R.id.layout_download);
        mCardImage = itemView.findViewById(R.id.cv_image);
        requestOptions = new RequestOptions();

        Drawable drawable = context.getResources().getDrawable(R.drawable.qiscus_circular_progress);
        mProgressImg.setProgress(0);   // Main Progress
        mProgressImg.setSecondaryProgress(100); // Secondary Progress
        mProgressImg.setMax(100); // Maximum Progress
        mProgressImg.setProgressDrawable(drawable);
        this.context = context;
    }

    @Override
    public void bind(QiscusComment comment) {
        super.bind(comment);

        mImageComment.setVisibility(View.VISIBLE);

        setJsonObj(comment);

        try {
            if (handler.getCaptionFile(obj).equals("")) {
                mTextChat.setVisibility(View.GONE);
            } else {
                mTextChat.setText(handler.getCaptionFile(obj));
                mTextChat.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        checkImage(comment);

        initClickListener(comment);
    }

    private void initClickListener(QiscusComment comment) {
        mLayoutDownload.setOnClickListener(v -> {
            if (obj != null) {
                try {
                    downloadFile(comment, handler.getNameFile(obj), handler.getUrlFile(obj));
                    hideOrShowView(mImageDownload, View.GONE);
                    hideOrShowView(mProgressImg, View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context, "Something Error", Toast.LENGTH_LONG).show();
            }
        });
        mImageComment.setOnClickListener(v -> {

            if (QiscusCore.getDataStore().getLocalPath(comment.getId()) != null) {
                String getFileName = "";
                try {
                    getFileName = handler.getNameFile(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(context, ShowImageActivity.class);
                intent.putExtra("sender", comment.getSender());
                intent.putExtra("image", generateFileDirectory() + getFileName);
                try {
                    intent.putExtra("message", handler.getCaptionFile(obj));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                context.startActivity(intent);
            }
        });

        mCardImage.setOnClickListener(v -> itemClickListener.onItemClick(v, getAdapterPosition()));

        mImageComment.setOnLongClickListener(v -> {
            longItemClickListener.onLongItemClick(v, getAdapterPosition());
            return false;
        });
    }

    private String generateFileDirectory() {
        return Environment.getExternalStorageDirectory().getPath() + File.separator + QiscusFileUtil.IMAGE_PATH + File.separator;
    }

    private void setJsonObj(QiscusComment comment) {
        try {
            obj = new JSONObject(comment.getExtraPayload());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkImage(QiscusComment comment) {
        try {
            JSONObject obj2 = new JSONObject(comment.getExtraPayload());
            if (handler.getUrlFile(obj2).startsWith("http")) { //We have sent it
                setImageToView(comment);
            } else { //Still uploading the image
                File file = new File(handler.getUrlFile(obj));

                showLocalImage(file);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setImageToView(QiscusComment comment) {
        File file = QiscusCore.getDataStore().getLocalPath(comment.getId());
        if (file != null) {
            showLocalImage(file);
        } else {
            showServerImage(comment);
        }
    }

    private void showServerImage(QiscusComment comment) {
        try {
            hideOrShowView(mLayoutCoverImg, View.VISIBLE);
            Nirmana.getInstance().get()
                    .load(QiscusImageUtil.generateBlurryThumbnailUrl(handler.getUrlFile(obj)))
                    .apply(requestOptions.dontAnimate().placeholder(R.drawable.ic_img_placeholder))
                    .into(mImageComment);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showLocalImage(File file) {
        hideOrShowView(mLayoutCoverImg, View.GONE);
        Nirmana.getInstance().get()
                .load(file)
                .apply(requestOptions.dontAnimate().placeholder(R.drawable.ic_img_placeholder))
                .into(mImageComment);
    }

    private void hideOrShowView(View view, int visibility) {
        view.setVisibility(visibility);
    }

    private void downloadFile(QiscusComment qiscusComment, String fileName, String url) {
        mImageDownload.setVisibility(View.GONE);
        mProgressImg.setVisibility(View.VISIBLE);
        mTextDownloadProgress.setVisibility(View.VISIBLE);
        QiscusApi.getInstance()
                .downloadFile(url, fileName, this::setProgress)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(file -> {
                    QiscusCore.getDataStore()
                            .addOrUpdateLocalPath(qiscusComment.getRoomId(), qiscusComment.getId(), file.getAbsolutePath());
                    QiscusImageUtil.addImageToGallery(file);
                })
                .doOnError(Throwable::printStackTrace)

                .subscribe(file -> {
                    hideOrShowView(mLayoutCoverImg, View.GONE);
                    setImageToView(qiscusComment);
                }, Throwable::printStackTrace);
    }

    private void setProgress(long total) {
        QiscusAndroidUtil.runOnUIThread(() -> {
            mProgressImg.setProgress((int) total);
            mTextDownloadProgress.setText(String.valueOf(total));
        });
    }
}

