package com.multichannel.chatcustomer.ui.adapter.viewholder;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.ui.adapter.OnItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnLongItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;
import com.multichannel.chatcustomer.util.QiscusImageUtil;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.data.remote.QiscusApi;
import com.qiscus.sdk.chat.core.util.QiscusAndroidUtil;
import com.qiscus.sdk.chat.core.util.QiscusFileUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class FileViewHolder extends RoomChatAdapter.ViewHolder {

    JSONObject obj;
    private TextView mTextFile;
    private ImageView mIconContent;
    private CardView mCvfile;
    private RelativeLayout mLayoutDownloadContainer;
    private RelativeLayout mLayoutDownload;
    private ProgressBar mProgressFile;
    private Context context;
    private String fileName;
    private boolean isLocal = false;

    public FileViewHolder(@NonNull View itemView, Context context, OnItemClickListener onItemClickListener,
                          OnLongItemClickListener onLongItemClickListener) {
        super(itemView, onItemClickListener, onLongItemClickListener);
        mTextFile = itemView.findViewById(R.id.tv_title_file);
        mIconContent = itemView.findViewById(R.id.iv_icon_file);
        mCvfile = itemView.findViewById(R.id.cv_file_content);
        mLayoutDownloadContainer = itemView.findViewById(R.id.layout_doc_download_container);
        mLayoutDownload = itemView.findViewById(R.id.layout_download);
        mProgressFile = itemView.findViewById(R.id.pb_file);

        Drawable drawable = context.getResources().getDrawable(R.drawable.qiscus_circular_progress);
        mProgressFile.setProgress(0);   // Main Progress
        mProgressFile.setSecondaryProgress(100); // Secondary Progress
        mProgressFile.setMax(100); // Maximum Progress
        mProgressFile.setProgressDrawable(drawable);

        this.context = context;
    }

    @Override
    public void bind(QiscusComment comment) {
        super.bind(comment);

        mTextChat.setVisibility(View.GONE);

        setJsonObj(comment);

        checkFile(comment);

        initClickListener(comment);
    }

    private void initClickListener(QiscusComment comment) {
        mCvfile.setOnClickListener(v -> {
            if (isLocal) {
                openFile();
            } else {
                try {
                    downloadFile(comment, handler.getNameFile(obj), handler.getUrlFile(obj));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void openFile() {
        String getFileName = "";
        try {
            getFileName = handler.getNameFile(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intentProvider(getFileName);
        } else {
            oldIntentMetode(getFileName);
        }
    }

    private void intentProvider(String getFileName) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Uri uri = FileProvider.getUriForFile(context, sharedPref.getProviderAuthorities(),
                new File(generateFileDirectory() + getFileName));

        intent.setDataAndType(uri, generateIntentType(getFileName));

        PackageManager pm = context.getPackageManager();
        if (intent.resolveActivity(pm) != null) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "Cannot Open File", Toast.LENGTH_LONG).show();
        }
    }

    private void oldIntentMetode(String getFileName) {
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(new File(generateFileDirectory() + File.separator + getFileName)),
                generateIntentType(getFileName));
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Choose App");

        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "Cannot Find Application to Open File", Toast.LENGTH_LONG).show();
        }
    }

    private String generateFileDirectory() {
        return Environment.getExternalStorageDirectory().getPath() + File.separator + QiscusFileUtil.FILES_PATH + File.separator;
    }

    private String generateIntentType(String fileName) {
        String extension = QiscusFileUtil.getExtension(fileName);
        String type = "";

        if (extension.equals("pdf")) {
            type = "application/pdf";
        } else if (extension.equals("doc") || extension.equals("docx") || extension.equals("rtf")) {
            type = "application/msword";
        } else if (extension.equals("xls") || extension.equals("xlsx")) {
            type = "application/vnd.ms-excel";
        } else if (extension.equals("ppt") || extension.equals("pptx")) {
            type = "application/vnd.ms-powerpoint";
        } else if (extension.equals("txt")) {
            type = "text/plain";
        } else {
            type = "";
        }

        return type;
    }

    private void checkFile(QiscusComment comment) {
        try {
            obj = new JSONObject(comment.getExtraPayload());
            if (handler.getUrlFile(obj).startsWith("http")) { //We have sent it
                setFile(comment);
            } else { //Still uploading the image
                File file = new File(handler.getUrlFile(obj));
                isLocal = true;

                showLocalFile(file);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setFile(QiscusComment comment) {
        File file = QiscusCore.getDataStore().getLocalPath(comment.getId());
        if (file != null) {
            showLocalFile(file);
        } else {
            showServerFile();
        }
    }

    private void showLocalFile(File file) {
        isLocal = true;
        hideOrShowView(mLayoutDownloadContainer, View.GONE);

        String textFileName = file.getName();
        if (file.getName().length() > 20) {
            textFileName = file.getName().substring(0, 20);
        }
        mTextFile.setText(textFileName);
        mTextFile.setText(file.getName());

        String extension = file.getName().substring(file.getName().lastIndexOf("."));
        setIconFile(mIconContent, extension);
    }

    private void showServerFile() {
        isLocal = false;
        hideOrShowView(mLayoutDownloadContainer, View.VISIBLE);
        try {
            fileName = handler.getNameFile(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String textFileName = fileName;
        if (fileName.length() > 20) {
            textFileName = fileName.substring(0, 20);
        }
        mTextFile.setText(textFileName);

        String extension = fileName.substring(fileName.lastIndexOf("."));
        setIconFile(mIconContent, extension);
    }

    private void setJsonObj(QiscusComment comment) {
        try {
            obj = new JSONObject(comment.getExtraPayload());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void hideOrShowView(View view, int visibility) {
        view.setVisibility(visibility);
    }

    private void downloadFile(QiscusComment qiscusComment, String fileName, String url) {
        hideOrShowView(mLayoutDownload, View.GONE);
        hideOrShowView(mProgressFile, View.VISIBLE);
        QiscusApi.getInstance()
                .downloadFile(url, fileName, this::setProgress)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(file -> {
                    QiscusCore.getDataStore()
                            .addOrUpdateLocalPath(qiscusComment.getRoomId(), qiscusComment.getId(), file.getAbsolutePath());

                    QiscusImageUtil.addImageToGallery(file);
                })
                .doOnError(Throwable::printStackTrace)
                .subscribe(file -> {
                    hideOrShowView(mLayoutDownloadContainer, View.GONE);
                    isLocal = true;
                }, throwable -> {

                });
    }

    private void setProgress(long total) {
        QiscusAndroidUtil.runOnUIThread(() -> mProgressFile.setProgress((int) total));
    }

    private void setIconFile(ImageView iv, String extension) {
        if (extension.equals(".txt")) {
            iv.setImageResource(R.drawable.ic_jupuk_txt);
        } else if (extension.equals(".pdf")) {
            iv.setImageResource(R.drawable.ic_jupuk_pdf);
        } else if (extension.equals(".ppt") || extension.equals(".pptx")) {
            iv.setImageResource(R.drawable.ic_jupuk_ppt);
        } else if (extension.equals(".doc") || extension.equals(".docx") || extension.equals(".rtf")) {
            iv.setImageResource(R.drawable.ic_jupuk_word);
        } else if (extension.equals(".xls") || extension.equals(".xlsx")) {
            iv.setImageResource(R.drawable.ic_jupuk_excel);
        } else {
            iv.setImageResource(R.drawable.ic_jupuk_file_unknown);
        }
    }
}
