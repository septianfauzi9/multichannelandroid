package com.multichannel.chatcustomer.ui.adapter.viewholder;

import android.annotation.SuppressLint;
import android.net.Uri;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.ui.adapter.OnItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnLongItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ButtonViewHolder extends RoomChatAdapter.ViewHolder {

    private TextView titleView, textContent, textTime;
    private ViewGroup buttonsContainer, messageContainer;

    private ChatButtonView.ChatButtonClickListener chatButtonClickListener;

    public ButtonViewHolder(View itemView, ChatButtonView.ChatButtonClickListener chatButtonClickListener,
                            OnItemClickListener onItemClickListener, OnLongItemClickListener onLongItemClickListener) {
        super(itemView, onItemClickListener, onLongItemClickListener);
        buttonsContainer = itemView.findViewById(R.id.buttons_container);
        messageContainer = itemView.findViewById(R.id.message);
        titleView = itemView.findViewById(R.id.tv_chat);
        textContent = itemView.findViewById(R.id.contents);
        textTime = itemView.findViewById(R.id.time);
        this.chatButtonClickListener = chatButtonClickListener;
    }

    @Override
    public void bind(QiscusComment comment) {
        super.bind(comment);
        try {
            JSONObject obj = new JSONObject(comment.getExtraPayload());
            setUpButtons(obj.getJSONArray("buttons"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        messageContainer.setBackground(
                ContextCompat.getDrawable(QiscusCore.getApps(),
                        R.drawable.rounded_primary_light_chat_bg));

        textContent.setText(comment.getMessage());
        textTime.setText(comment.getTime().toString());
    }

    @SuppressLint("NewApi")
    private void setUpButtons(JSONArray buttons) {
        buttonsContainer.removeAllViews();

        int size = buttons.length();
        if (size < 1) {
            return;
        }
        List<ChatButtonView> buttonViews = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            try {
                JSONObject jsonButton = buttons.getJSONObject(i);
                String type = jsonButton.optString("type", "");
                if ("postback".equals(type)) {
                    ChatButtonView button = new ChatButtonView(buttonsContainer.getContext(), jsonButton);
                    button.setChatButtonClickListener(chatButtonClickListener);
                    buttonViews.add(button);
                } else if ("link".equals(type)) {
                    ChatButtonView button = new ChatButtonView(buttonsContainer.getContext(), jsonButton);
                    button.setChatButtonClickListener(jsonButton1 ->
                            openLink(jsonButton1.optJSONObject("payload").optString("url")));
                    buttonViews.add(button);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < buttonViews.size(); i++) {
            buttonViews.get(i).getButton().setTextColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.font_color));

            if (i == buttonViews.size() - 1) {
                buttonViews.get(i).getButton().setBackground(itemView.getContext().getDrawable(R.drawable.button_chat_bg));
            } else {
                buttonViews.get(i).getButton().setBackgroundColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.white));
            }

            buttonsContainer.addView(buttonViews.get(i));
        }

        buttonsContainer.setVisibility(View.VISIBLE);
    }

    private void openLink(String url) {
        new CustomTabsIntent.Builder()
                .setToolbarColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.colorPrimary))
                .setShowTitle(true)
                .addDefaultShareMenuItem()
                .enableUrlBarHiding()
                .build()
                .launchUrl(buttonsContainer.getContext(), Uri.parse(url));
    }


}
