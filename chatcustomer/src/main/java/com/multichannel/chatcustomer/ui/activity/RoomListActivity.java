package com.multichannel.chatcustomer.ui.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.presenter.MainChatMvp;
import com.multichannel.chatcustomer.presenter.imp.MainChatPresenter;
import com.multichannel.chatcustomer.ui.adapter.RoomListAdapter;
import com.multichannel.chatcustomer.util.Constant;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.model.QiscusAccount;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.event.QiscusCommentReceivedEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RoomListActivity extends AppCompatActivity implements MainChatMvp {

    public static boolean isActive = false;

    private RecyclerView mRecData;
    private ProgressBar progressBar;
    private NestedScrollView nestedScrollView;
    private TextView mTextToolbarTitle;
    private CircleImageView mImageAvatar;
    private ImageView mImageEmpty;
    private FloatingActionButton mFabQismo;

    private ProgressDialog progressDialog;
    private QiscusAccount account;
    private RequestOptions requestOptions;
    private RoomListAdapter adapter;
    private MainChatPresenter presenter;
    private List<QiscusChatRoom> chatRoom = new ArrayList<>();

    private boolean isLastPage = false;
    private int page = 1;
    private int limit = 100;
    private boolean isLoading = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_chat_customer);

        initCommponent();
    }

    @Override
    public void initCommponent() {
        getDataIntent();

        presenter = new MainChatPresenter();
        presenter.onAttachView(this);
        presenter.setLimit(limit);

        requestOptions = new RequestOptions();
        adapter = new RoomListAdapter(this);

        findView();
        setProfile();
        setupList();
    }

    @Override
    public void findView() {
        mTextToolbarTitle = findViewById(R.id.tv_toolbar_title);
        mImageAvatar = findViewById(R.id.iv_avatar);
        mImageEmpty = findViewById(R.id.iv_empty_room);
        nestedScrollView = findViewById(R.id.nsv_ListChat);
        mRecData = findViewById(R.id.rv_ChatList);
        progressBar = findViewById(R.id.pb_load_list);
        mFabQismo = findViewById(R.id.fab);

        initClickListener();
    }

    private void initClickListener() {
        mFabQismo.setOnClickListener(v -> {
            QiscusAccount qiscusAccount = QiscusCore.getQiscusAccount();

            Intent i = new Intent(RoomListActivity.this, RoomChatActivity.class);
            i.putExtra("name", qiscusAccount.getUsername());
            i.putExtra("email", qiscusAccount.getEmail());
            startActivity(i);
        });

        mImageAvatar.setOnClickListener(v -> openSettings());
        mTextToolbarTitle.setOnClickListener(v -> openSettings());
    }

    private void getDataIntent() {
        try {
            Intent i = getIntent();
            account = i.getParcelableExtra(Constant.ACOUNT_DATA);
        } catch (Exception e) {
            showError("gagal mengambil data intent");
        }
    }

    @Override
    public void setProfile() {
        Nirmana.getInstance().get()
                .load(account.getAvatar())
                .apply(requestOptions.dontAnimate().placeholder(R.drawable.ic_img_placeholder))
                .into(mImageAvatar);
        mTextToolbarTitle.setText(account.getUsername());
    }

    @Override
    public void setupList() {
        mRecData.setHasFixedSize(true);
        mRecData.setAdapter(adapter);
        onScroll();
    }

    private void openSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("avatar", account.getAvatar());
        intent.putExtra("username", account.getUsername());
        startActivity(intent);
    }

    @SuppressLint("NewApi")
    private void onScroll() {

//        nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (nestedScrollView, i, i1, i2, i3) -> {
//            if (adapter.isLoadCheck() && i1 > i3) {
//                if (i1 == (nestedScrollView.getChildAt(0).getMeasuredHeight() - nestedScrollView.getMeasuredHeight())) {
//                    limit = limit + limit;
//                    page = page + 1;
//                    presenter.setPage(page);
//                    presenter.getSetupList();
//                }
//            }
//        });
    }

    @Override
    public void setRoomList(List<QiscusChatRoom> qiscusChatRoom) {
        if (qiscusChatRoom == null) {
            showError("Something Error");
            dissmissPbLoad();
        } else {
            chatRoom.addAll(qiscusChatRoom);

            if (chatRoom.size() == 0) {
                mImageEmpty.setVisibility(View.VISIBLE);
                dismissLoading();
                dissmissPbLoad();
            } else {
                mImageEmpty.setVisibility(View.GONE);
                if (chatRoom.size() == qiscusChatRoom.size()) {
                    adapter.addOrUpdate(qiscusChatRoom);
                    isLoading = true;
                    dissmissPbLoad();
                } else if (chatRoom.size() == adapter.getItemCount()) {
                    if (!isLoading) {
                        dissmissPbLoad();
                    }
                } else {
                    adapter.addOrUpdate(chatRoom);
                    isLoading = false;
                }
                if (!isLastPage && adapter.getItemCount() <= limit) {
                    presenter.setPage(page++);
//                    presenter.getSetupList();
                }
            }

        }
        dismissLoading();
    }

    @Override
    public void setIsLastPage(boolean isLastPage) {
        this.isLastPage = isLastPage;
    }

    @Override
    public void showLoading() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
        }
        progressDialog.show();
    }

    @Override
    public void dismissLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showPbLoad() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dissmissPbLoad() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onCommentReceivedEvent(QiscusCommentReceivedEvent event) {
//        presenter.getSetupList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        page = 0;
        presenter.setPage(page);
//        Enable this to show SDK chat list
//        presenter.getSetupList();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
