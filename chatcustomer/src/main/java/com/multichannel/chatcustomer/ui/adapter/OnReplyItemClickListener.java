package com.multichannel.chatcustomer.ui.adapter;

import com.qiscus.sdk.chat.core.data.model.QiscusComment;

public interface OnReplyItemClickListener {
    void onReplyItemClick(QiscusComment comment);
}
