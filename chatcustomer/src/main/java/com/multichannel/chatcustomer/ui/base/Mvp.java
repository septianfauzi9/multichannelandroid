package com.multichannel.chatcustomer.ui.base;

/**
 * Created by adicatur on 12/24/16.
 */

public interface Mvp {

    void showLoading();

    void dismissLoading();

    void showError(String error);

}
