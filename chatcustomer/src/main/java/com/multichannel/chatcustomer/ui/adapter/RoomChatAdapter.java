package com.multichannel.chatcustomer.ui.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.database.SharedPref;
import com.multichannel.chatcustomer.handler.JsonParsingHandler;
import com.multichannel.chatcustomer.ui.adapter.viewholder.ButtonViewHolder;
import com.multichannel.chatcustomer.ui.adapter.viewholder.CardViewHolder;
import com.multichannel.chatcustomer.ui.adapter.viewholder.CarouselItemView;
import com.multichannel.chatcustomer.ui.adapter.viewholder.CarouselViewHolder;
import com.multichannel.chatcustomer.ui.adapter.viewholder.ChatButtonView;
import com.multichannel.chatcustomer.ui.adapter.viewholder.FileViewHolder;
import com.multichannel.chatcustomer.ui.adapter.viewholder.ImageViewHolder;
import com.multichannel.chatcustomer.ui.adapter.viewholder.ReplyViewHolder;
import com.multichannel.chatcustomer.ui.adapter.viewholder.SystemEventViewHolder;
import com.multichannel.chatcustomer.ui.adapter.viewholder.TextViewHolder;
import com.multichannel.chatcustomer.util.DateUtil;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.util.QiscusAndroidUtil;
import com.qiscus.sdk.chat.core.util.QiscusDateUtil;

import java.util.ArrayList;
import java.util.List;

public class RoomChatAdapter extends SortedRecyclerViewAdapter<QiscusComment, RoomChatAdapter.ViewHolder> {

    private static final int ROOM_COMMENT = 1;
    private static final int ROOM_COMMENT_ME = 2;
    private static final int ROOM_COMMENT_IMAGE = 3;
    private static final int ROOM_COMMENT_IMAGE_ME = 4;
    private static final int ROOM_COMMENT_FILE = 5;
    private static final int ROOM_COMMENT_FILE_ME = 6;
    private static final int ROOM_COMMENT_CAROUSEL_ME = 7;
    private static final int ROOM_COMMENT_CAROUSEL = 8;
    private static final int ROOM_COMMENT_BUTTONS = 9;
    private static final int ROOM_COMMENT_BUTTONS_ME = 10;
    private static final int ROOM_COMMENT_CARD = 11;
    private static final int ROOM_COMMENT_CARD_ME = 12;
    private static final int ROOM_COMMENT_SYSTEM_EVENT = 13;
    private static final int ROOM_COMMENT_REPLY = 14;
    private static final int ROOM_COMMENT_REPLY_ME = 15;
    private Context context;
    private ChatButtonView.ChatButtonClickListener chatButtonClickListener;
    private CarouselItemView.ChatItemClickListener itemClickListener;
    private CardViewHolder.ChatItemClickListener cardItemClickListener;
    private OnLongItemClickListener longItemClickListener;
    private OnItemClickListener adapterItemClickListener;
    private OnReplyItemClickListener replyItemClickListener;

    private long lastDeliveredCommentId;
    private long lastReadCommentId;

    public RoomChatAdapter(Context context,
                           ChatButtonView.ChatButtonClickListener chatButtonClickListener,
                           CarouselItemView.ChatItemClickListener itemClickListener,
                           CardViewHolder.ChatItemClickListener cardItemClickListener) {
        this.context = context;

        this.chatButtonClickListener = chatButtonClickListener;
        this.itemClickListener = itemClickListener;
        this.cardItemClickListener = cardItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        switch (i) {
            case ROOM_COMMENT:
                return new TextViewHolder(getViewType(viewGroup, i), adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_ME:
                return new TextViewHolder(getViewType(viewGroup, i), adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_IMAGE:
                return new ImageViewHolder(getViewType(viewGroup, i), context, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_IMAGE_ME:
                return new ImageViewHolder(getViewType(viewGroup, i), context, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_FILE:
                return new FileViewHolder(getViewType(viewGroup, i), context, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_FILE_ME:
                return new FileViewHolder(getViewType(viewGroup, i), context, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_CAROUSEL:
                return new CarouselViewHolder(getViewType(viewGroup, i), chatButtonClickListener, itemClickListener, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_CAROUSEL_ME:
                return new CarouselViewHolder(getViewType(viewGroup, i), chatButtonClickListener, itemClickListener, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_BUTTONS:
                return new ButtonViewHolder(getViewType(viewGroup, i), chatButtonClickListener, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_BUTTONS_ME:
                return new ButtonViewHolder(getViewType(viewGroup, i), chatButtonClickListener, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_CARD:
                return new CardViewHolder(getViewType(viewGroup, i), chatButtonClickListener, cardItemClickListener, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_CARD_ME:
                return new CardViewHolder(getViewType(viewGroup, i), chatButtonClickListener, cardItemClickListener, adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_SYSTEM_EVENT:
                return new SystemEventViewHolder(getViewType(viewGroup, i), adapterItemClickListener, longItemClickListener);
            case ROOM_COMMENT_REPLY:
            case ROOM_COMMENT_REPLY_ME:
                return new ReplyViewHolder(getViewType(viewGroup, i), adapterItemClickListener, longItemClickListener, replyItemClickListener);
            default:
                return new TextViewHolder(getViewType(viewGroup, i), adapterItemClickListener, longItemClickListener);
        }
    }

    private View getViewType(ViewGroup viewGroup, int i) {
        switch (i) {
            case ROOM_COMMENT_ME:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_text_me, viewGroup, false);
            case ROOM_COMMENT:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_text, viewGroup, false);
            case ROOM_COMMENT_IMAGE:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_image, viewGroup, false);
            case ROOM_COMMENT_IMAGE_ME:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_image_me, viewGroup, false);
            case ROOM_COMMENT_FILE:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_file, viewGroup, false);
            case ROOM_COMMENT_FILE_ME:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_file_me, viewGroup, false);
            case ROOM_COMMENT_CAROUSEL:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_carousel, viewGroup, false);
            case ROOM_COMMENT_CAROUSEL_ME:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_carousel_me, viewGroup, false);
            case ROOM_COMMENT_BUTTONS:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_button, viewGroup, false);
            case ROOM_COMMENT_BUTTONS_ME:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_button_me, viewGroup, false);
            case ROOM_COMMENT_CARD:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_card, viewGroup, false);
            case ROOM_COMMENT_CARD_ME:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_card_me, viewGroup, false);
            case ROOM_COMMENT_SYSTEM_EVENT:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_system_event, viewGroup, false);
            case ROOM_COMMENT_REPLY:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_reply, viewGroup, false);
            case ROOM_COMMENT_REPLY_ME:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_reply_me, viewGroup, false);
            default:
                return LayoutInflater.from(context).inflate(R.layout.item_chat_text_me, viewGroup, false);
        }
    }

    public void addOrUpdate(List<QiscusComment> comments) {
        for (QiscusComment comment : comments) {
            int index = findPosition(comment);
            if (index == -1) {
                getData().add(comment);
            } else {
                getData().updateItemAt(index, comment);
            }
        }
        notifyDataSetChanged();
    }

    public void addOrUpdate(QiscusComment qiscusComment) {
        int index = findPosition(qiscusComment);
        if (index == -1) {
            getData().add(qiscusComment);
        } else {
            getData().updateItemAt(index, qiscusComment);
        }
        notifyDataSetChanged();
    }

    public void remove(QiscusComment comment) {
        getData().remove(comment);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        getData().removeItemAt(position);
        notifyItemRemoved(position);
    }

    public void removePendingComment(QiscusComment qiscusComment) {
        int size = getData().size();
        for (int i = 0; i < size; i++) {
            QiscusComment comment = getData().get(i);
            if (comment.getId() == -1) {
                getData().remove(comment);
                addOrUpdate(qiscusComment);
            }
        }
    }

    public QiscusComment getLatestSentComment() {
        int size = getData().size();
        for (int i = 0; i < size; i++) {
            QiscusComment comment = getData().get(i);
            if (comment.getState() >= QiscusComment.STATE_ON_QISCUS) {
                return comment;
            }
        }
        return null;
    }

    public void updateLastDeliveredComment(long lastDeliveredCommentId) {
        this.lastDeliveredCommentId = lastDeliveredCommentId;
        updateCommentState();
        notifyDataSetChanged();
    }

    public void updateLastReadComment(long lastReadCommentId) {
        this.lastReadCommentId = lastReadCommentId;
        this.lastDeliveredCommentId = lastReadCommentId;
        updateCommentState();
        notifyDataSetChanged();
    }

    private void updateCommentState() {
        int size = getData().size();
        for (int i = 0; i < size; i++) {
            if (getData().get(i).getState() > QiscusComment.STATE_SENDING) {
                if (getData().get(i).getId() <= lastReadCommentId) {
                    if (getData().get(i).getState() == QiscusComment.STATE_READ) {
                        break;
                    }
                    getData().get(i).setState(QiscusComment.STATE_READ);
                } else if (getData().get(i).getId() <= lastDeliveredCommentId) {
                    if (getData().get(i).getState() == QiscusComment.STATE_DELIVERED) {
                        break;
                    }
                    getData().get(i).setState(QiscusComment.STATE_DELIVERED);
                }
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        QiscusComment comment = getData().get(i);
        holder.bind(comment);
        if (i == getData().size() - 1) {
            holder.showFirstMessageIndicator(holder.setNeedToShowDate(true), getData(), i);
        } else {
            holder.showFirstMessageIndicator(
                    holder.setNeedToShowDate(
                            !QiscusDateUtil.isDateEqualIgnoreTime(
                                    getData().get(i).getTime(),
                                    getData().get(i + 1).getTime())),
                    getData(), i);
        }

    }

    @Override
    protected Class<QiscusComment> getItemClass() {
        return QiscusComment.class;
    }

    @Override
    protected int compare(QiscusComment item1, QiscusComment item2) {
        if (item2.equals(item1)) { //Same comments
            return 0;
        } else if (item2.getId() == -1 && item1.getId() == -1) { //Not completed comments
            return item2.getTime().compareTo(item1.getTime());
        } else if (item2.getId() != -1 && item1.getId() != -1) { //Completed comments
            return QiscusAndroidUtil.compare(item2.getId(), item1.getId());
        } else if (item2.getId() == -1) {
            return 1;
        } else if (item1.getId() == -1) {
            return -1;
        }
        return item2.getTime().compareTo(item1.getTime());
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    @Override
    public int getItemViewType(int position) {
        QiscusComment qiscusComment = getData().get(position);

        switch (qiscusComment.getType()) {
            case TEXT:
                return qiscusComment.isMyComment() ? ROOM_COMMENT_ME : ROOM_COMMENT;
            case FILE:
                return qiscusComment.isMyComment() ? ROOM_COMMENT_FILE_ME : ROOM_COMMENT_FILE;
            case CAROUSEL:
                return qiscusComment.isMyComment() ? ROOM_COMMENT_CAROUSEL_ME : ROOM_COMMENT_CAROUSEL;
            case BUTTONS:
                return qiscusComment.isMyComment() ? ROOM_COMMENT_BUTTONS_ME : ROOM_COMMENT_BUTTONS;
            case CARD:
                return qiscusComment.isMyComment() ? ROOM_COMMENT_CARD_ME : ROOM_COMMENT_CARD;
            case SYSTEM_EVENT:
                return ROOM_COMMENT_SYSTEM_EVENT;
            case IMAGE:
                return qiscusComment.isMyComment() ? ROOM_COMMENT_IMAGE_ME : ROOM_COMMENT_IMAGE;
            case REPLY:
                return qiscusComment.isMyComment() ? ROOM_COMMENT_REPLY_ME : ROOM_COMMENT_REPLY;
            default:
                return qiscusComment.isMyComment() ? ROOM_COMMENT_ME : ROOM_COMMENT;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<QiscusComment> getSelectedComments() {
        List<QiscusComment> selectedComments = new ArrayList<>();
        int size = getData().size();
        for (int i = size - 1; i >= 0; i--) {
            if (getData().get(i).isSelected()) {
                selectedComments.add(getData().get(i));
            }
        }
        return selectedComments;
    }

    public void clearSelectedComments() {
        int size = getData().size();
        for (int i = size - 1; i >= 0; i--) {
            if (getData().get(i).isSelected()) {
                getData().get(i).setSelected(false);
            }
        }
        notifyDataSetChanged();
    }

    public void setOnLongItemClickListener(OnLongItemClickListener longItemClickListener) {
        this.longItemClickListener = longItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.adapterItemClickListener = itemClickListener;
    }

    public void setOnReplyItemClickListener(OnReplyItemClickListener replyItemClickListener) {
        this.replyItemClickListener = replyItemClickListener;
    }

    public interface RecyclerViewItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener{

        protected TextView mTextChat;
        protected JsonParsingHandler handler;
        protected SharedPref sharedPref;
        private TextView mTextDate;
        private ImageView mChatFrom;
        private TextView time;
        @Nullable
        private ImageView state;

        private int pendingStateColor;
        private int readStateColor;
        private int failedStateColor;
        protected Drawable selectionBackground;

        protected OnItemClickListener itemClickListener;
        protected OnLongItemClickListener longItemClickListener;

        public ViewHolder(@NonNull View itemView, OnItemClickListener itemClickListener,
                          OnLongItemClickListener longItemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            this.longItemClickListener = longItemClickListener;

            mTextChat = itemView.findViewById(R.id.tv_chat);
            mTextDate = itemView.findViewById(R.id.tv_date);
            mChatFrom = itemView.findViewById(R.id.chat_from);
            time = itemView.findViewById(R.id.time);
            state = itemView.findViewById(R.id.state);

            pendingStateColor = ContextCompat.getColor(itemView.getContext(), R.color.pending_message);
            readStateColor = ContextCompat.getColor(itemView.getContext(), R.color.read_message);
            failedStateColor = ContextCompat.getColor(itemView.getContext(), R.color.qiscus_red);

            selectionBackground = new ColorDrawable(ContextCompat.getColor(QiscusCore.getApps(), R.color.qiscus_primary_light));
            selectionBackground.setAlpha(51);

            handler = JsonParsingHandler.init();
            sharedPref = new SharedPref(itemView.getContext());

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void bind(QiscusComment comment) {

            if (comment.getMessage() != null) {
                mTextChat.setText(comment.getMessage());
            }

            if (mTextDate != null) {
                mTextDate.setText(DateUtil.toFullDate(comment.getTime()));
            }

            if (time != null) {
                time.setText(DateUtil.getTimeStringFromDate(comment.getTime()));
            }

            renderState(comment);

            checkSelectedComment(comment);
        }

        private void checkSelectedComment(QiscusComment comment) {
            itemView.setBackground(comment.isSelected() || comment.isHighlighted() ? selectionBackground : null);
        }

        boolean setNeedToShowDate(Boolean showDate) {
            if (showDate) {
                if (mTextDate != null) {
                    mTextDate.setVisibility(View.VISIBLE);
                }
            } else {
                if (mTextDate != null) {
                    mTextDate.setVisibility(View.GONE);
                }
            }
            return showDate;
        }

        void showFirstMessageIndicator(boolean showDate, SortedList<QiscusComment> data, int position) {
            if (showDate || data.get(position + 1).getType() == QiscusComment.Type.CARD
                    || data.get(position + 1).getType() == QiscusComment.Type.CAROUSEL
                    || data.get(position + 1).getType() == QiscusComment.Type.SYSTEM_EVENT) {
                setChatFrom(true);
            } else if (data.get(position).getSenderEmail().equals(data.get(position + 1).getSenderEmail())) {
                setChatFrom(false);
            } else {
                setChatFrom(true);
            }
        }

        private void setChatFrom(boolean chatFrom) {
            if (mChatFrom != null) {
                mChatFrom.setVisibility(View.GONE);
                if (chatFrom) {
                    mChatFrom.setVisibility(View.VISIBLE);
                }
            }
        }

        private void renderState(QiscusComment comment) {
            if (state != null) {
                switch (comment.getState()) {
                    case QiscusComment.STATE_PENDING:
                    case QiscusComment.STATE_SENDING:
                        state.setColorFilter(pendingStateColor);
                        state.setImageResource(R.drawable.ic_qiscus_info_time);
                        break;
                    case QiscusComment.STATE_ON_QISCUS:
                        state.setColorFilter(pendingStateColor);
                        state.setImageResource(R.drawable.ic_qiscus_sending);
                        break;
                    case QiscusComment.STATE_DELIVERED:
                        state.setColorFilter(pendingStateColor);
                        state.setImageResource(R.drawable.ic_qiscus_read);
                        break;
                    case QiscusComment.STATE_READ:
                        state.setColorFilter(readStateColor);
                        state.setImageResource(R.drawable.ic_qiscus_read);
                        break;
                    case QiscusComment.STATE_FAILED:
                        state.setColorFilter(failedStateColor);
                        state.setImageResource(R.drawable.ic_qiscus_sending_failed);
                        break;
                }
            }
        }

        @Override
        public void onClick(View v) {
            if (v.equals(itemView)) {
                int position = getAdapterPosition();
                if (position >= 0) {
                    itemClickListener.onItemClick(v, position);
                }
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (longItemClickListener != null) {
                int position = getAdapterPosition();
                if (position >= 0) {
                    longItemClickListener.onLongItemClick(v, position);
                }
                return true;
            }
            return false;
        }
    }
}
