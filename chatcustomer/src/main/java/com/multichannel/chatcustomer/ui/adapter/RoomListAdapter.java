package com.multichannel.chatcustomer.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.ui.activity.RoomChatActivity;
import com.multichannel.chatcustomer.util.DateUtil;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RoomListAdapter extends SortedRecyclerViewAdapter<QiscusChatRoom, RoomListAdapter.MyViewHolder> {

    private Context context;
    private boolean isLoad = false;
    private RequestOptions requestOptions;

    public RoomListAdapter(Context context) {
        this.context = context;
        requestOptions = new RequestOptions();

    }

    public boolean isLoadCheck() {
        return isLoad;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_list_chat, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        QiscusChatRoom room = getData().get(i);

        holder.bind(room);
    }

    public void addOrUpdate(List<QiscusChatRoom> chatRooms) {
        for (QiscusChatRoom chatRoom : chatRooms) {
            int index = findPosition(chatRoom);
            if (index == -1) {
                getData().add(chatRoom);
            } else {
                getData().updateItemAt(index, chatRoom);
            }
        }
        notifyDataSetChanged();
    }



    private void startChat(long roomId) {
        Intent intent = new Intent(context, RoomChatActivity.class);
        intent.putExtra("roomId", roomId);
        context.startActivity(intent);

        //Room Cat Family 1379470
        //Room Lazarus - Miza 1918511
    }

    @Override
    protected Class<QiscusChatRoom> getItemClass() {
        return QiscusChatRoom.class;
    }

    @Override
    protected int compare(QiscusChatRoom item1, QiscusChatRoom item2) {
        return item2.getLastComment().getTime().compareTo(item1.getLastComment().getTime());
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView iv;
        private TextView mTextNama, mTextDetail, mTextDate;
        private TextView mTextUnreadCount;
        private ViewGroup item;

        MyViewHolder(@NonNull View v) {
            super(v);
            iv = v.findViewById(R.id.iv_imgAvatar);
            mTextNama = v.findViewById(R.id.tv_UserName);
            mTextDetail = v.findViewById(R.id.tv_UserDetail);
            mTextDate = v.findViewById(R.id.tv_DataMessage);
            mTextUnreadCount = v.findViewById(R.id.tv_unread_count);
            item = v.findViewById(R.id.item_list);
        }

        public void removeItemView() {
            itemView.setVisibility(View.GONE);
        }

        public void bind(QiscusChatRoom room) {
            Nirmana.getInstance().get()
                    .load(room.getAvatarUrl())
                    .apply(requestOptions.dontAnimate().placeholder(R.drawable.ic_img_placeholder))
                    .into(iv);
            mTextNama.setText(room.getName());
            mTextDetail.setText(room.getLastComment().getMessage());
            mTextDate.setText(DateUtil.getLastMessageTimestamp(room.getLastComment().getTime()));
            long roomId = room.getLastComment().getRoomId();

            mTextUnreadCount.setText(String.valueOf(room.getUnreadCount()));

            if (room.getUnreadCount() > 0) {
                mTextUnreadCount.setVisibility(View.VISIBLE);
            } else {
                mTextUnreadCount.setVisibility(View.GONE);
            }

            item.setOnClickListener(view -> startChat(roomId));
        }
    }
}
