package com.multichannel.chatcustomer.ui.adapter.viewholder;

import android.annotation.SuppressLint;
import android.net.Uri;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.ui.adapter.OnItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnLongItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.chat.core.QiscusCore;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CardViewHolder extends RoomChatAdapter.ViewHolder {

    private View itemView;
    private CardView cardView;
    private ImageView imageView;
    private TextView titleView;
    private TextView descriptionView;
    private LinearLayout buttonsContainer;

    private ChatButtonView.ChatButtonClickListener chatButtonClickListener;
    private ChatItemClickListener itemClickListener;

    public CardViewHolder(View itemView, ChatButtonView.ChatButtonClickListener chatButtonClickListener,
                          ChatItemClickListener itemClickListener, OnItemClickListener onItemClickListener,
                          OnLongItemClickListener onLongItemClickListener) {
        super(itemView, onItemClickListener, onLongItemClickListener);
        this.itemView = itemView;
        cardView = itemView.findViewById(R.id.card_view);
        imageView = itemView.findViewById(R.id.thumbnail);
        titleView = itemView.findViewById(R.id.title);
        descriptionView = itemView.findViewById(R.id.description);
        buttonsContainer = itemView.findViewById(R.id.buttons_container);
        setChatItemClickListener(itemClickListener);
        setChatButtonClickListener(chatButtonClickListener);
    }

    @Override
    public void bind(QiscusComment comment) {
        super.bind(comment);
        try {
            JSONObject obj = new JSONObject(comment.getExtraPayload());
            render(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void render(JSONObject payload) {
        cardView.setOnClickListener(v -> {
            openLink(payload.optString("url"));
        });

        RequestOptions requestOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(R.drawable.qiscus_image_placeholder)
                .error(R.drawable.qiscus_image_placeholder);

        Nirmana.getInstance().get()
                .setDefaultRequestOptions(requestOptions)
                .load(payload.optString("image", ""))
                .into(imageView);

        titleView.setText(payload.optString("title"));
        titleView.setTextColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.font_color));
        descriptionView.setText(payload.optString("description", ""));

        try {
            setUpButtons(payload.getJSONArray("buttons"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openLink(String url) {
        new CustomTabsIntent.Builder()
                .setToolbarColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.colorPrimary))
                .setShowTitle(true)
                .addDefaultShareMenuItem()
                .enableUrlBarHiding()
                .build()
                .launchUrl(itemView.getContext(), Uri.parse(url));
    }

    @SuppressLint("ResourceAsColor")
    private void setUpButtons(JSONArray buttons) {
        buttonsContainer.removeAllViews();

        int size = buttons.length();
        if (size < 1) {
            return;
        }
        List<ChatButtonView> buttonViews = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            try {
                JSONObject jsonButton = buttons.getJSONObject(i);
                String type = jsonButton.optString("type", "");
                if ("postback".equals(type)) {
                    ChatButtonView button = new ChatButtonView(buttonsContainer.getContext(), jsonButton);
                    button.setChatButtonClickListener(chatButtonClickListener);
                    buttonViews.add(button);
                } else if ("link".equals(type)) {
                    ChatButtonView button = new ChatButtonView(buttonsContainer.getContext(), jsonButton);
                    button.setChatButtonClickListener(jsonButton1 ->
                            openLink(jsonButton1.optJSONObject("payload").optString("url")));
                    buttonViews.add(button);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < buttonViews.size(); i++) {
            buttonViews.get(i).getButton().setBackgroundColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.white));
            buttonViews.get(i).getButton().setTextColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.font_color));
            buttonsContainer.addView(buttonViews.get(i));
        }

        buttonsContainer.setVisibility(View.VISIBLE);
    }

    public void setChatButtonClickListener(ChatButtonView.ChatButtonClickListener chatButtonClickListener) {
        this.chatButtonClickListener = chatButtonClickListener;
    }

    public void setChatItemClickListener(ChatItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ChatItemClickListener {
        void onChatItemClick(JSONObject jsonButton);
    }

}
