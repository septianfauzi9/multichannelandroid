package com.multichannel.chatcustomer.ui.activity;

import com.qiscus.sdk.chat.core.data.model.QiscusComment;

import java.util.List;

public interface CommentSelectedListener {
    void onCommentSelected(List<QiscusComment> selectedComments);
}
