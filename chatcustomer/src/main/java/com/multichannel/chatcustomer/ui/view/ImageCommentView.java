package com.multichannel.chatcustomer.ui.view;

import androidx.core.util.Pair;

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;

import java.util.List;

public interface ImageCommentView {
    void setRoomData(Pair<QiscusChatRoom, List<QiscusComment>> qiscusChatRoomListPair);
    void uploadSuccess();
    void errorMessage(String error);
}
