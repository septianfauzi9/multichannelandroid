package com.multichannel.chatcustomer.ui.view;

import androidx.core.util.Pair;

import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.data.model.QiscusRoomMember;

import java.util.Date;
import java.util.List;

public interface RoomChatView {
    void setRoomData(Pair<QiscusChatRoom, List<QiscusComment>> qiscusChatRoomListPair);
    void setOlderComment(List<QiscusComment> comments);
    void setUploadProgress(long progress);
    void uploadSuccess();
    void sendingComment(QiscusComment qiscusComment);
    void errorMessage(String error);

    void onNewComment(QiscusComment qiscusComment);
    void updateLastDeliveredMessage(long lastDeliveredCommentId);
    void updateLastReadComment(long lastReadCommentId);

    void onCommentDeleted(QiscusComment qiscusComment);

    void refreshComment(QiscusComment qiscusComment);

    void onRealtimeStatusChanged(boolean connected);

    void onLoadMore(List<QiscusComment> comments);

    void onUserTyping(boolean typing);

    void setOnlineStatus(boolean online, Date lastActive);

    void setParticipants(List<QiscusRoomMember> member);

    void dismissLoading();

    void showError(String error);

    void setMessageBoxForQismo(boolean b);

    void onCommentSuccess(QiscusComment qiscusComment);

    void showCommentsAndScrollToTop(List<QiscusComment> comments);

    void onFailedSendComment(QiscusComment qiscusComment);

    void setRoomResolved(boolean b);

    void setLastCommentWhenRoomResolved(QiscusComment qiscusComment);
}
