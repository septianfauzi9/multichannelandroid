package com.multichannel.chatcustomer.ui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.request.RequestOptions;
import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.handler.RepliedJsonParsingHandler;
import com.multichannel.chatcustomer.ui.adapter.OnItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnLongItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnReplyItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;

import org.json.JSONException;
import org.json.JSONObject;

public class ReplyViewHolder extends RoomChatAdapter.ViewHolder{

    private TextView mTextRepliedUser;
    private TextView mTextRepliedMessage;
    private ImageView mImageRepliedImage;
    private RelativeLayout mLayoutReply;

    private JSONObject obj;
    private RepliedJsonParsingHandler repliedJsonHandler;

    private OnReplyItemClickListener replyItemClickListener;

    public ReplyViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener,
                           OnLongItemClickListener onLongItemClickListener, OnReplyItemClickListener replyItemClickListener) {
        super(itemView, onItemClickListener, onLongItemClickListener);

        this.replyItemClickListener = replyItemClickListener;

        mTextRepliedUser = itemView.findViewById(R.id.tv_replied_username);
        mTextRepliedMessage = itemView.findViewById(R.id.tv_replied_message);
        mImageRepliedImage = itemView.findViewById(R.id.iv_replied_image);
        mLayoutReply = itemView.findViewById(R.id.layout_reply);

        repliedJsonHandler = new RepliedJsonParsingHandler();
    }

    @Override
    public void bind(QiscusComment comment) {
        super.bind(comment);

        setObj(comment);

        setRepliedView(comment);

        setClickListener(comment);
    }

    private void setClickListener(QiscusComment comment) {
        mLayoutReply.setOnClickListener(v -> {
            replyItemClickListener.onReplyItemClick(comment);
        });
    }

    private void setRepliedView(QiscusComment comment) {
        try {
            mTextRepliedUser.setText(repliedJsonHandler.getRepliedMessageUserName(obj));

            if (repliedJsonHandler.getRepliedMessageType(obj).equals("file_attachment")) {
                mTextRepliedMessage.setText(repliedJsonHandler.getFileName(obj));
                Nirmana.getInstance()
                        .get()
                        .applyDefaultRequestOptions(new RequestOptions().dontAnimate().placeholder(R.drawable.ic_img_placeholder))
                        .load(repliedJsonHandler.getUrlFile(obj))
                        .into(mImageRepliedImage);

                mImageRepliedImage.setVisibility(View.VISIBLE);
            } else {
                mTextRepliedMessage.setText(repliedJsonHandler.getRepliedMessageText(obj));
                mImageRepliedImage.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setObj(QiscusComment comment) {
        try {
            obj = new JSONObject(comment.getExtraPayload());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
