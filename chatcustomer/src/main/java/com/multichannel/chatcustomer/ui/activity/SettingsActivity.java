package com.multichannel.chatcustomer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.request.RequestOptions;
import com.multichannel.chatcustomer.R;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.chat.core.QiscusCore;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {

    private RelativeLayout mLayoutLogout;
    private TextView mTextToolbarTitle;
    private CircleImageView mImageAvatar;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        init();
    }

    private void init() {

        mLayoutLogout = findViewById(R.id.layout_logout);
        mTextToolbarTitle = findViewById(R.id.tv_toolbar_title);
        mImageAvatar = findViewById(R.id.iv_avatar);
        mImageBack = findViewById(R.id.iv_back);

        Intent intent = getIntent();

        if (intent != null) {
            Nirmana.getInstance().get()
                    .load(intent.getStringExtra("avatar"))
                    .apply(new RequestOptions().dontAnimate().placeholder(R.drawable.ic_img_placeholder))
                    .into(mImageAvatar);
            mTextToolbarTitle.setText(intent.getStringExtra("username"));
        }

        mLayoutLogout.setOnClickListener(v -> {
            QiscusCore.clearUser();
            finishAffinity();
        });

        mImageBack.setOnClickListener(v -> finish());
    }
}
