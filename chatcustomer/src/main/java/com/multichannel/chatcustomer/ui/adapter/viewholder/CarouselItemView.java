package com.multichannel.chatcustomer.ui.adapter.viewholder;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.multichannel.chatcustomer.R;
import com.qiscus.nirmana.Nirmana;
import com.qiscus.sdk.chat.core.QiscusCore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CarouselItemView extends FrameLayout {

    private CardView cardView;
    private ImageView imageView;
    private TextView titleView;
    private TextView descriptionView;
    private LinearLayout buttonsContainer;

    private JSONObject payload;

    private ChatButtonView.ChatButtonClickListener chatButtonClickListener;
    private ChatItemClickListener itemClickListener;

    public CarouselItemView(Context context) {
        super(context);
        injectViews();
    }

    private void injectViews() {
        inflate(getContext(), R.layout.view_carousel_item, this);
        cardView = findViewById(R.id.card_view);
        imageView = findViewById(R.id.thumbnail);
        titleView = findViewById(R.id.title);
        descriptionView = findViewById(R.id.description);
        buttonsContainer = findViewById(R.id.buttons_container);
    }

    public void setPayload(JSONObject payload) {
        this.payload = payload;
    }

    public void render() {
        cardView.setOnClickListener(v -> {
            JSONObject action = payload.optJSONObject("default_action");
            String type = action.optString("type");
            if (type.equals("postback")) {
                if (itemClickListener != null) {
                    itemClickListener.onChatItemClick(action);
                }
            } else if (type.equals("link")) {
                openLink(action.optJSONObject("payload").optString("url"));
            }
        });

        RequestOptions requestOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(R.drawable.qiscus_image_placeholder)
                .error(R.drawable.qiscus_image_placeholder);

        Nirmana.getInstance().get()
                .setDefaultRequestOptions(requestOptions)
                .load(payload.optString("image", ""))
                .into(imageView);

        titleView.setText(payload.optString("title"));
        titleView.setTextColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.font_color));
        descriptionView.setText(payload.optString("description", ""));

        try {
            setUpButtons(payload.getJSONArray("buttons"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openLink(String url) {
        new CustomTabsIntent.Builder()
                .setToolbarColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.colorPrimary))
                .setShowTitle(true)
                .addDefaultShareMenuItem()
                .enableUrlBarHiding()
                .build()
                .launchUrl(getContext(), Uri.parse(url));
    }

    @SuppressLint("ResourceAsColor")
    private void setUpButtons(JSONArray buttons) {
        buttonsContainer.removeAllViews();

        int size = buttons.length();
        if (size < 1) {
            return;
        }
        List<ChatButtonView> buttonViews = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            try {
                JSONObject jsonButton = buttons.getJSONObject(i);
                String type = jsonButton.optString("type", "");
                if ("postback".equals(type)) {
                    ChatButtonView button = new ChatButtonView(buttonsContainer.getContext(), jsonButton);
                    button.setChatButtonClickListener(chatButtonClickListener);
                    buttonViews.add(button);
                } else if ("link".equals(type)) {
                    ChatButtonView button = new ChatButtonView(buttonsContainer.getContext(), jsonButton);
                    button.setChatButtonClickListener(jsonButton1 ->
                            openLink(jsonButton1.optJSONObject("payload").optString("url")));
                    buttonViews.add(button);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < buttonViews.size(); i++) {
            buttonViews.get(i).getButton().setBackgroundColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.white));
            buttonViews.get(i).getButton().setTextColor(ContextCompat.getColor(QiscusCore.getApps(), R.color.font_color));
            buttonsContainer.addView(buttonViews.get(i));
        }

        buttonsContainer.setVisibility(View.VISIBLE);
    }

    public void setChatButtonClickListener(ChatButtonView.ChatButtonClickListener chatButtonClickListener) {
        this.chatButtonClickListener = chatButtonClickListener;
    }

    public void setChatItemClickListener(CarouselItemView.ChatItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ChatItemClickListener {
        void onChatItemClick(JSONObject jsonButton);
    }

}
