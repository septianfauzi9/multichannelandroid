package com.multichannel.chatcustomer.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.helper.QiscusZoomableImageView;

import java.io.File;

public class ShowImageActivity extends AppCompatActivity {

    private ImageView mImageBack;
    private QiscusZoomableImageView mImageContent;
    private TextView mTextToolbar;
    private TextView mTextMessage;
    private RelativeLayout mLayoutMessage;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image_customer);

        init();

    }

    private void init() {
        mImageBack = findViewById(R.id.iv_back);
        mImageContent = findViewById(R.id.iv_content_image);
        mTextToolbar = findViewById(R.id.tv_toolbar_title);
        mTextMessage = findViewById(R.id.tv_message);
        mLayoutMessage = findViewById(R.id.layout_message);
        mToolbar = findViewById(R.id.toolbar);

        Intent intent = getIntent();

        if (intent != null) {
            String senderName = intent.getStringExtra("sender");
            String imageUri = intent.getStringExtra("image");
            String message = intent.getStringExtra("message");

            if (senderName != null && !senderName.equals("")) {
                mTextToolbar.setText(senderName);
            }

            if (imageUri != null && !imageUri.equals("")) {
                Glide.with(this)
                        .asBitmap()
                        .load(Uri.fromFile(new File(imageUri)))
                        .apply(new RequestOptions().centerCrop())
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                mImageContent.setImageBitmap(resource);
                            }
                        });
            }

            if (message != null && !message.equals("")) {
                mLayoutMessage.setVisibility(View.VISIBLE);
                mTextMessage.setText(message);
            }
        }

        initClickListener();
    }

    private void hideOrShowViewImageView(int visibility) {
        mToolbar.setVisibility(visibility);
        if (!mTextMessage.getText().equals(""))
            mLayoutMessage.setVisibility(visibility);
    }

    private void initClickListener() {
        mImageBack.setOnClickListener(v -> {
            finish();
        });

        mImageContent.setOnClickListener(v -> {
            hideOrShowViewImageView(mToolbar.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
        });

        mImageContent.setOnTouchImageViewListener(new QiscusZoomableImageView.OnTouchImageViewListener() {
            @Override
            public void onMove() {
                mImageContent.printMatrixInfo();
            }
        });
    }
}
