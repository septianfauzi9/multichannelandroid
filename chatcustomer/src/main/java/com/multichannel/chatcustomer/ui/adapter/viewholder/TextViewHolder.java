package com.multichannel.chatcustomer.ui.adapter.viewholder;

import androidx.annotation.NonNull;
import android.view.View;

import com.multichannel.chatcustomer.ui.adapter.OnItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnLongItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;

public class TextViewHolder extends RoomChatAdapter.ViewHolder {

    public TextViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener,
                          OnLongItemClickListener onLongItemClickListener) {
        super(itemView, onItemClickListener, onLongItemClickListener);
    }

    @Override
    public void bind(QiscusComment comment) {
        super.bind(comment);

        mTextChat.setText(comment.getMessage());
    }
}

