package com.multichannel.chatcustomer.ui.adapter.viewholder;

import android.view.View;
import android.view.ViewGroup;

import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.ui.adapter.OnItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.OnLongItemClickListener;
import com.multichannel.chatcustomer.ui.adapter.RoomChatAdapter;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CarouselViewHolder extends RoomChatAdapter.ViewHolder {

    private ViewGroup cardContainer;
    private ChatButtonView.ChatButtonClickListener chatButtonClickListener;
    private CarouselItemView.ChatItemClickListener itemClickListener;

    public CarouselViewHolder(View itemView, ChatButtonView.ChatButtonClickListener chatButtonClickListener,
                              CarouselItemView.ChatItemClickListener itemClickListener,
                              OnItemClickListener onItemClickListener, OnLongItemClickListener onLongItemClickListener) {
        super(itemView, onItemClickListener, onLongItemClickListener);
        cardContainer = itemView.findViewById(R.id.cards_container);
        this.chatButtonClickListener = chatButtonClickListener;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void bind(QiscusComment comment) {
        super.bind(comment);
        try {
            JSONObject obj = new JSONObject(comment.getExtraPayload());
            setUpCards(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setUpCards(JSONObject obj) {
        JSONArray cards = obj.optJSONArray("cards");
        int size = 0;
        try {
            size = cards.length();
        } catch (Exception ignore) {
        }
        cardContainer.removeAllViews();
        for (int i = 0; i < size; i++) {
            try {
                CarouselItemView itemView = new CarouselItemView(cardContainer.getContext());
                itemView.setPayload(cards.getJSONObject(i));
                itemView.setChatButtonClickListener(chatButtonClickListener);
                itemView.setChatItemClickListener(itemClickListener);
                itemView.render();
                cardContainer.addView(itemView);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        cardContainer.setVisibility(View.VISIBLE);
    }
}

