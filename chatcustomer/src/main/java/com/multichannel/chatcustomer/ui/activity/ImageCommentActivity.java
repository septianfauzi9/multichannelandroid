package com.multichannel.chatcustomer.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Pair;

import com.bumptech.glide.Glide;
import com.multichannel.chatcustomer.R;
import com.multichannel.chatcustomer.ui.view.ImageCommentView;
import com.multichannel.chatcustomer.util.Constant;
import com.qiscus.sdk.chat.core.data.model.QiscusChatRoom;
import com.qiscus.sdk.chat.core.data.model.QiscusComment;
import com.qiscus.sdk.chat.core.util.QiscusFileUtil;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ImageCommentActivity extends AppCompatActivity implements ImageCommentView {

    private ImageView mImageBack;
    private ImageView mImageSend;
    private EditText mEditComment;
    private ImageView mImageAvatar;
    private ImageView mImageContent;
    private TextView mTextToolbarTitle;

    private Intent i;
    private File file;
    private String data;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_image_comment);
        getIntentData();
        findView();
        setSupportActionBar(toolbar);
        setImage();
    }

    private void initClickListener() {
        mImageSend.setOnClickListener(v -> sendImage());
        mImageBack.setOnClickListener(v -> closeActivity());
    }

    private void findView() {
        toolbar = findViewById(R.id.toolbar);
        mEditComment = findViewById(R.id.field_setimg_message);
        mImageAvatar = findViewById(R.id.iv_avatar);
        mTextToolbarTitle = findViewById(R.id.tv_toolbar_title);
        mImageContent = findViewById(R.id.iv_CheckImg);
        mImageBack = findViewById(R.id.iv_back);
        mImageSend = findViewById(R.id.iv_send);

        initClickListener();
    }

    private void getIntentData() {
        i = getIntent();

        if (i != null) {
            data = i.getStringExtra(Constant.DATA);
        }
    }

    private void setImage() {
        runOnUiThread(() -> {
            try {
                file = QiscusFileUtil.from(Uri.parse(data));
                Uri uri = Uri.fromFile(file);
                mImageContent.setImageURI(uri);
            } catch (IOException e) {
                Toast.makeText(this, "Failed to open image file!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void setRoomData(Pair<QiscusChatRoom, List<QiscusComment>> qiscusChatRoomListPair) {
        mTextToolbarTitle.setText(qiscusChatRoomListPair.first.getName());
        Glide.with(this)
                .load(qiscusChatRoomListPair.first.getAvatarUrl())
                .into(mImageAvatar);
    }

    @Override
    public void uploadSuccess() {

    }

    @Override
    public void errorMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    public void sendImage() {
        String comment = mEditComment.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("uri", data);
        intent.putExtra("comment", comment);

        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public void closeActivity() {
        finish();
    }

}
