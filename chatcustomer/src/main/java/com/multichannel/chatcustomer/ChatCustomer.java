package com.multichannel.chatcustomer;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.multichannel.chatcustomer.data.remote.QismoWidgetApi;
import com.multichannel.chatcustomer.database.SharedPref;
import com.multichannel.chatcustomer.ui.activity.RoomChatActivity;
import com.multichannel.chatcustomer.util.Constant;
import com.qiscus.jupuk.Jupuk;
import com.qiscus.sdk.chat.core.QiscusCore;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChatCustomer {

    public static final String QISCUS_PROVIDER = ".qiscus.multichannel";


    private static SharedPref sharedPref;
    private Context context;

    public ChatCustomer(Context context) {
        this.context = context;
    }

    public static void init(Application application, String qiscusAppId) {
        QiscusCore.init(application, qiscusAppId);
        Jupuk.init(application);
    }

    public static void initProviderAuthority(Context context, String providerAuthority) {
        sharedPref = new SharedPref(context);
        sharedPref.setProviderAuthorities(providerAuthority);
    }

    public static void initiateChat(Context context1, String userId, String username, String phone, InitiateCallback initiateCallback) {
        QismoWidgetApi.getInstance().initiateChat(userId, username, phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(account -> {
                    Intent i = new Intent(context1, RoomChatActivity.class);
                    i.putExtra(Constant.ACOUNT_DATA, QiscusCore.getQiscusAccount());
                    i.putExtra("name", username);
                    i.putExtra("email", userId);
                    i.putExtra("phone",phone);
                    context1.startActivity(i);
                    initiateCallback.onSuccess();
                }, throwable -> {
                    throwable.printStackTrace();
                    Log.d("selama","selama");
                    initiateCallback.onError(throwable);
                });

    }

    private static void showError(Context context1, String errorMessage) {
        Toast.makeText(context1, errorMessage, Toast.LENGTH_SHORT).show();
    }

    public interface InitiateCallback {
        void onSuccess();
        void onError(Throwable throwable);
    }

}
